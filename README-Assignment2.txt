README txt file!

To Run the program, open the window console and navigate to the folder with
the ImageApp.exe inside of it. (It is inside of the VisualStudio solution Debug folder)


Alternatively, you can open the VS 2015 community edition project and
run the project inside the editor. If you run the project inside the editor (using crtl+f5),
RightClick the ImageApp Project -> Properties -> Configuration Properties -> Debugging and use the
Command Arguments box to control the input to the program. 




-------Equalize function:------------
Use this function to equalize images and display histograms.

ImageAppe.exe sourceImage targetImage Equalize -R -G -B -H -S -I -HG -Q

the -R,-G,-B,-H,-S,-I,-HG,-Q are all optional and don't need to be in any particular order.

Equalize a Grayscale image example:
ImageApp.exe source target Equalize -R

Equalize Grayscale image with histogram outputs
ImageApp.exe source target Equalize -R -HG

Equalize Grayscale image with histogram outputs and into quarters
ImageApp.exe source target Equalize -R -HG -Q

Equalize on R and G channels
ImageApp.exe source target Equalize -R -G

Equalize on R channel
ImageApp.exe source target Equalize -R

Equalize on H and S and I channels with histogram
ImageApp.exe source target Equalize -H -S -I -HG

Equalize B channel into quarters
ImageApp.exe source target Equalize -B -Q


Notes***
-HG outputs before and after histogram for each channel that was equalized.
  a command -HG -S -I -H will output 24 histogram images. ( 8 for each channel)

-RGB and HSI equalization cannot occur on the same command. Any HSI equalization request will
override all RGB requests. To equalize RGB then HSI, run an RGB equalize command, and then run an HSI command.

-Q applys quarters on all channels specified in the command


----- Stretching -----

Example:
ImageApp.exe source target Stretch

This will output a target image, a before-stretching histogram, and an after-stretching histogram.





------- AddHSI ------

Adds the input HSI vector to an image.
ImageApp.exe source target H S I
where H,S,I are intiger numbers

Example:
ImageApp.exe source target AddHSI 0 0 50
will intensify the image by 50

ImageApp.exe source target AddHSI 10 50 -50
will subtract intesity and add contrast














