README txt file!

To Run the program, open the window console and navigate to the folder with
the ImageApp.exe inside of it. (It is inside of the VisualStudio solution Debug folder)


Alternatively, you can open the VS 2015 community edition project and
run the project inside the editor. If you run the project inside the editor (using crtl+f5),
RightClick the ImageApp Project -> Properties -> Configuration Properties -> Debugging and use the
Command Arguments box to control the input to the program. 


Only one new function this time...

-------EdgeDetect function:--------

This function outputs a gradient-amplitude image, a thresholded gradient-amplitude image, 
and a directional-thresholded gradient image. It outputs three images for each channel the algorithm is run on.
==================================================================================================================
Function:
ImageApp.exe sourceImage EdgeDetect [T] [Td] -R -G -B -H -S -I -3x3 -C -ROI [X][Y][L][W]...[Xn][Yn][Ln][Wn]
==================================================================================================================
[bracketed] identifiers represent intiger parameters
parameters with a '-' are optional parameters.
==================================================================================================================
[T] : user threshold
[Td] : user directional threshold

-R,-G,-B,-H,-S,-I : optional channels to run the algorithm on. The function will not do HSI and RGB at 
                    the same time. Atleast one channel needs to be selected or nothing will happen.

-3x3 : Use a 3x3 filter instead of the default 5x5 filter.

-C : combine the results of the selected channel into one image. (Only implemented for RGB, HSI combining not required)

-ROI [X][Y][L][W]...[Xn][Yn][Ln][Wn] : Add regions of interest to the image.
                                       The ROI option must be the last option used in running the program.
     [X][Y] : starting pixel of ROI
     [L][W] : length and width of the ROI
==================================================================================================================

Examples:

ImageApp.exe sourceImage EdgeDetect 10 90 -R
-runs edge detect over the redchannel. Run the program with -R option for gray-level images.
-uses threshold of '10', angle of '90'

ImageApp.exe sourceImage EdgeDetect 10 90 -R -G -B
-runs edge detect over R,G,B channels and outputs 3 results for each channel.
-uses threshold of '10', angle of '90'

ImageApp.exe sourceImage EdgeDetect 15 0 -R -G -B -C
-runs edge detect over R,G,B channels and outputs 3 results for each channel.
-outputs a combination image of R,G,B
-uses threshold of '15', angle of '0'

ImageApp.exe sourceImage EdgeDetect 15 0 -R -G -C
-runs edge detect over R,G channels and outputs 3 results for each channel.
-outputs a combination image of R,G
-uses threshold of '15', angle of '0'

ImageApp.exe sourceImage EdgeDetect 10 90 -H -S -I -3x3
-runs edge detect over H,S,I and uses a 3x3 filter instead of the default 5x5 filter
-uses threshold of '10', angle of '90'

ImageApp.exe sourceImage EdgeDetect 10 90 -I
-runs edge detect over I
-uses threshold of '10', angle of '90'

ImageApp.exe sourceImage EdgeDetect 5 45 -I -ROI 0 10 200 300
-runs edge detect over I, includes a single ROI located at (x=0,y=10) with a widthxheight of (w=200,h=300)
-uses threshold of '5', angle of '45'


ImageApp.exe sourceImage EdgeDetect 5 45 -I -S -H -ROI 0 10 200 300 200 200 200 200 0 400 10 20
-runs edge detect over I,S,H includes multiple ROI
-uses threshold of '5', angle of '45'




