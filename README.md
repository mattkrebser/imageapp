# README #

ImageApp

### What is this repository for? ###

* Image processing tools to modify .PPM or .PGM images.
* Console Application
* Histogram equalization, RGB & HSI transformations, Edge Detection, thresholding, image scaling, image smoothing, histogram stretching, 
* Version 1.0

### How do I get set up? ###

* Summary of set up
* Download entire repository, it is a visual studio project folder
* Open in visual studio and compile the program
* Requires opencv version 3.1 or later
  * Read the 'README-Assignment4' .txt file for opencv instructions. Alternatively, you can just delete the opencv code from the project and remove
references in the project properties.  
  * ******Read all of the READMEs in the project folder before running or building******


![ImageAppscrnshot.png](https://bitbucket.org/repo/zkG5nq/images/3565265932-ImageAppscrnshot.png)