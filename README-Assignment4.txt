README txt file!


This version of the project requires OpenCV to be installed on your computer!




---OPENCV SPECIFICATIONS---------------------------------------------------------------------------
My project uses a system variable to access the opencv directory.
The variable is named 'OPENCV_DIR'.
It may be easiest to create a system variable named 'OPENCV_DIR' that specifies the directory of your opencv 
installation.

In my setup, the system path to opencv assumes a path similar to:
c:\..user_files..\opencv\build\x86\vc14
or for x64
c:\..user_files..\opencv\build\x64\vc14

My program is run and tested with the version 3.1 of OpenCV, that was compiled for VisualStudio 2015

My Visual Studio Solution assumes an opencv directory setup similar to
 v3.1 of opencv (in order to access obj,lib,dll files)

If you are not using V3.1 of OPENCV (or have a weird opencv file structure),
then you may have to manually change the project properties
in order to correctly link with the OpenCV object files.

I have tested my program with both x86 and x64 architecture, so either will work.
My project is currently active on the x86 architecture. I included executables for
both x86 and x64

You probably don't need them, but this is very similar to how I installed OpenCV
https://www.youtube.com/watch?v=EcFtefHEEII&nohtml5=False
https://www.youtube.com/watch?v=tHX3MLzwF6Q&nohtml5=False
----------------------------------------------------------------------------------------------------




******RUN*******************************************************************************************
To Run the program, open the window console and navigate to the folder with
the ImageApp.exe inside of it. (It is inside of the VisualStudio solution Debug folder)


Alternatively, you can open the VS 2015 community edition project and
run the project inside the editor. If you run the project inside the editor (using crtl+f5),
RightClick the ImageApp Project -> Properties -> Configuration Properties -> Debugging and use the
Command Arguments box to control the input to the program. 
****************************************************************************************************





Histogram Equalization----------------------------------------------------------------------------
Inputs an image, outputs an equalized image and histograms displaying before and after.

Function:
ImageApp.exe input.pgm output.pgm OCV -Eq -Q

input.pgm  : input image
output.pgm : output image name
OCV        : required for all OpenCV functions
-Eq        : Option used for equalization
-Q         : option for equalization, using this option will do histogram equalization in quarters

Examples:

//without quarters
ImageApp.exe input.pgm output.pgm OCV -Eq
//with quarters
ImageApp.exe input.pgm output.pgm OCV -Eq -Q
------------------------------------------------------------------------------------------------------------------





Edge Dection (Sobel)----------------------------------------------------------------------------------------------------
Inputs an Image, outputs the edge gradient amplitude, tresholded gradient, and thesholded direction&gradient images

Function:
ImageApp.exe input.pgm output.pgm OCV [T] [Td] -Sobel -ROI [RxRyRwRh]...[RxRyRwRh]

input.pgm  : input image
output.pgm : output image name
OCV        : required for all OpenCV functions
-Sobel     : Option to specify Sobel function
-ROI       : Optional region of interest
T          : Threshold for gradient
Td         : Directional threshold
Rx,Ry      : Region starting pixel
Rw,Rh      : Region width, Region Height

Examples:

//Do sobel edge detection with threshold of T=15, and directional threhsold of 45 degrees
ImageApp.exe input.pgm output.pgm OCV 15 45 -Sobel

//Same as above, but uses a region of interest
ImageApp.exe input.pgm output.pgm OCV 15 45 -Sobel -ROI 0 0 200 200

//Same as above, but uses many regions of interest
ImageApp.exe input.pgm output.pgm OCV 15 45 -Sobel -ROI 0 0 200 200 201 201 50 50 300 100 50 50
-------------------------------------------------------------------------------------------------------------------



Canny Edge Detection-----------------------------------------------------------------------------------------------
Inputs an image, outputs an image with edge detection

Function:
ImageApp.exe input.pgm output.pgm OCV [P1] [P2] -Canny -ROI [RxRyRwRh]...[RxRyRwRh]

input.pgm  : input image
output.pgm : output image name
OCV        : required for all OpenCV functions
-Canny     : Option to specify Canny function
-ROI       : Optional region of interest
P1,P2      : Canny parameters
Rx,Ry      : Region starting pixel
Rw,Rh      : Region width, Region Height

Examples:

//does canny edge detection with parameters 30,120
ImageApp.exe input.pgm output.pgm OCV 30 120 -Canny

//does canny edge detection with parameters 30,120 and a region of interest
ImageApp.exe input.pgm output.pgm OCV 30 120 -Canny -ROI 0 0 200 200

//does canny edge detection with parameters 30,120 and many regions of interest
ImageApp.exe input.pgm output.pgm OCV 30 120 -Canny -ROI 0 0 200 200 201 201 50 50 300 100 50 50
--------------------------------------------------------------------------------------------------------------------









