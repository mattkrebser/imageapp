To Run the program, open the window console and navigate to the folder with
the ImageApp.exe inside of it. 


Alternatively, you can open the VS 2015 community edition project and
run the project inside the editor. If you run the project inside the editor,
RightClick the ImageApp Project -> Properties -> Configuration Properties -> Debugging and use the
Command Arguments box to control the input to the program. 



--Image Scaling--

Image Scaling scales images by powers of 2, for example
using an input scale factor of 1 will scale an image by 2^1,
using an input scale factor of 2 will scale an image by 2^2, and so on...

To Scale all Images in a Directory:
ImageApp.exe TargetDirectoryPath ResultsDirectoryPath DirectoryScale ScaleFactor

Example: >ImageApp.exe MyDirectory NewDirectory DirectoryScale 1
Example: >ImageApp.exe MyDirectory NewDirectory DirectoryScale 2

To Scale a Single Image
ImageApp.exe MyImageName NewImageName Scale ScaleFactor

Example: >ImageApp.exe MyImageName NewImageName Scale 1





--Binarization--

To Binarize a directory:
ImageApp.exe MyImage NewImage DirectoryThreshold Threshold

Example>ImageApp.exe MyImage NewImage DirectoryThreshold 127

To Binarize an Image:
ImageApp.exe MyImage NewImage Binarize Threshold

Example>ImageApp.exe MyImage NewImage Binarize 127






--Color Binarization--

To Color Binarize a (PPM) Image:
ImageApp.exe MyImage NewImage ColorBinarize R G B T
where rgb is the color and T is the distance

Example>ImageApp.exe MyImage NewImage ColorBinarize 127 127 127 127





--2D Smoothing--

To Smooth an Image:
ImageApp.exe MyImage NewImage Smooth WindowSize

Example>ImageApp.exe MyImage NewImage Smooth 3






--Seperable Smoothing--

To Smooth an Image:
ImageApp.exe MyImage NewImage SeperableSmooth WindowSize

Example>ImageApp.exe MyImage NewImage SeperableSmooth 3






--Incremental Smoothing--

To Smooth an Image:
ImageApp.exe MyImage NewImage IncrementalSmooth WindowSize

Example>ImageApp.exe MyImage NewImage IncrementalSmooth 3




-- Region of Interest Operations--

To use ROI:
ImageApp.exe MyImage NewImage ROI Operation OperationParameter(s) NumRegions  (repeat foreach region)x0 y0 L W

Example Binarize (1 ROI):

>ImageApp.exe MyImage NewImage ROI Binarize 127 1 0 0 100 100

>ImageApp.exe MyImage NewImage ROI Binarize 127          1              0 0                 100 100
                                             ^Threshold  ^numRegions    ^upperleft corner    ^ Length x Width


2 regions of interest:
ImageApp.exe MyImage NewImage ROI Binarize 127    2     0 0 100 100       0 0 100 100 
      
3 regions of interest:
ImageApp.exe MyImage NewImage ROI Binarize 127    3     0 0 100 100       0 0 100 100       0 0 100 100

4 regions of interest:
ImageApp.exe MyImage NewImage ROI Binarize 127    4     0 0 100 100       0 0 100 100       0 0 100 100     0 0 100 100


Example ColorBinarize 

ImageApp.exe MyImage NewImage ROI ColorBinarize 127 127 127 80 1 0 0 100 100
                                                     
ImageApp.exe MyImage NewImage ROI ColorBinarize    127 127 127   80          1                0 0 100 100
                                                    ^RGB color    ^distance   ^numRegions     ^rectangle

4 Regions of Interest:
ImageApp.exe MyImage NewImage ROI ColorBinarize 127 127 127 80    4    0 0 100 100    0 0 100 100    0 0 100 100     0 0 100 100


Example 2d Smooth:
Smooth with W=3 and 4 ROI
ImageApp.exe MyImage NewImage ROI Smooth  3  4   0 0 100 100    0 0 100 100    0 0 100 100     0 0 100 100

Example IncrementalSmooth:
IncrementalSmooth with W=3 and 4 ROI
ImageApp.exe MyImage NewImage ROI IncrementalSmooth  3  4   0 0 100 100    0 0 100 100    0 0 100 100     0 0 100 100

Example SeperableSmooth:
Smooth with W=3 and 4 ROI
ImageApp.exe MyImage NewImage ROI SeperableSmooth  3  4   0 0 100 100    0 0 100 100    0 0 100 100     0 0 100 100





All of the algorithms are implemented on utility.cpp as static functions




