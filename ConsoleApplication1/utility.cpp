#include "stdafx.h"
#include "utility.h"
#include <math.h>
#include <string>

#include <windows.h>
#include <iostream>


float Pow2(int y);
bool dirExists(const std::string& dirName_in);
void ListAllFilesInDir(const char *directory, vector<string> &fnames);
void OutPutHistogram(string filename, vector<float> &pmf);
void Equalize(image &src, image &tgt, int rgb, int hsi, int row, int col, int rowlen, int collen);

/*-----------------------------------------------------------------------**/
void utility::addGrey(image &src, image &tgt, int value)
{
	tgt.resize(src.Width(), src.Height());
	for (int i = 0; i<src.Width(); i++)
		for (int j = 0; j<src.Height(); j++)
		{
			tgt.setPixel(i, j, src.getPixel(i, j) + value);

			if (tgt.getPixel(i, j) > 255)
				tgt.setPixel(i, j, 255);
			else if (tgt.getPixel(i, j) < 0)
				tgt.setPixel(i, j, 0);
		}
}

void utility::Threshold(image &src, image &tgt, int value) 
{
	tgt.resize(src.Width(), src.Height());
	for (int i = 0; i<src.Width(); i++)
		for (int j = 0; j < src.Height(); j++)
		{
			if (src.getPixel(i, j) >= value) {
				//set white
				tgt.setPixel(i, j, RED, 255);
				if (src.isPPM) { tgt.setPixel(i, j, GREEN, 255);tgt.setPixel(i, j, BLUE, 255); }
			}
			else {
				//set black
				tgt.setPixel(i, j, RED, 0);
				if (src.isPPM) { tgt.setPixel(i, j, GREEN, 0);tgt.setPixel(i, j, BLUE, 0);}
			}
		}
}

void utility::Scale(image &src, image &tgt, int factor)
{
	//if expanding
	if (factor >= 1) 
	{
		float pow2 = Pow2(factor);
		int mult = (int)pow2;
		tgt.resize((int)(src.Width() * mult), (int)(src.Height() * mult));

		for (int i = 0; i< src.Width(); i++)
			for (int j = 0; j < src.Height(); j++)
			{
				tgt.setPixels(i * mult, j * mult, RED, mult, mult, src.getPixel(i, j, RED));
				if (src.isPPM) 
				{
					tgt.setPixels(i * mult, j * mult, GREEN, mult, mult, src.getPixel(i, j, GREEN));
					tgt.setPixels(i * mult, j * mult, BLUE, mult, mult, src.getPixel(i, j, BLUE));
				}
			}
	}
	//decreasing
	else if (factor <= -1)
	{
		float pow2 = Pow2(factor);
		tgt.resize((int)(src.Width() * pow2), (int)(src.Height() * pow2));
		int mult = (int)(1.0f / pow2);
		for (int i = 0; i< tgt.Width(); i++)
			for (int j = 0; j < tgt.Height(); j++)
			{
				int mi = 0, mj = 0;
				mi = i * mult >= src.Width() ? src.Width() - 1 : i * mult;
				mj = j * mult >= src.Height() ? src.Height() - 1 : j * mult;

				tgt.setPixel(i, j, RED, src.Average(mi, mj, RED, mult));
				if (src.isPPM) 
				{
					tgt.setPixel(i, j, GREEN, src.Average(mi, mj, GREEN, mult));
					tgt.setPixel(i, j, BLUE, src.Average(mi, mj, BLUE, mult));
				}
			}
	}	
}

void utility::ColorBinarize(image &src, image &tgt, RGB color, int distance) 
{
	if (!src.isPPM) {
		cout << "Error, ColorBinarize is only for colored images!\n";
		exit(1);
	}

	tgt.resize(src.Width(), src.Height());
	RGB black(0, 0, 0); RGB white(255, 255, 255);

	for (int i = 0; i < tgt.Width(); i++) 
	{
		for (int j = 0; j < tgt.Height(); j++) 
		{
			RGB srcColor = src.getColorVector(i, j);
			if (RGB::distance(srcColor, color) > distance)
			{
				tgt.setColorVector(i, j, black);
			}
			else 
			{
				tgt.setColorVector(i, j, white);
			}
		}
	}
}

void utility::DirectoryScale(int factor, char * dirName, char * outputDirName) 
{
	if (dirExists(outputDirName)) 
	{
		cout << "Error, the output directory " << outputDirName << " already exists.\n";
		exit(1);
	}
	if (!dirExists(dirName)) 
	{
		cout << "Error, the input directory " << dirName << " does not exist.\n";
		exit(1);
	}
	vector<string> fnames;

	ListAllFilesInDir(dirName, fnames);

	if (CreateDirectoryA(outputDirName, NULL))
	{
		for (size_t i = 0; i < fnames.size(); i++)
		{
			image src, tgt;

			//get soruce path
			string path(dirName);
			path.append("\\");
			path.append(fnames[i]);

			//initialize source image
			src.read(&path[0]);
			tgt.isPPM = src.isPPM;

			//compute
			Scale(src, tgt, factor);

			//get output path
			string outpath(outputDirName);
			outpath.append("\\");
			outpath.append(fnames[i]);

			//save
			tgt.save(&outpath[0]);

			cout << "Finished new image at " << outpath << " " << i << "/" << fnames.size() << "\n";

			outpath.clear();
			path.clear();
		}
	}
	else
	{
		cout << "An error occured while making " << outputDirName << " directory";
		exit(1);
	}

}

void utility::DirectoryThreshold(int value, char * dirName, char * outputDirName)
{
	if (dirExists(outputDirName))
	{
		cout << "Error, the output directory " << outputDirName << " already exists.\n";
		exit(1);
	}
	if (!dirExists(dirName))
	{
		cout << "Error, the input directory " << dirName << " does not exist.\n";
		exit(1);
	}
	vector<string> fnames;

	ListAllFilesInDir(dirName, fnames);

	if (CreateDirectoryA(outputDirName, NULL))
	{
		for (size_t i = 0; i < fnames.size(); i++) 
		{
			image src, tgt;

			//get soruce path
			string path(dirName);
			path.append("\\");
			path.append(fnames[i]);

			//initialize source image
			src.read(&path[0]);
			tgt.isPPM = src.isPPM;

			//compute
			Threshold(src, tgt, value);

			//get output path
			string outpath(outputDirName);
			outpath.append("\\");
			outpath.append(fnames[i]);

			//save
			tgt.save(&outpath[0]);

			cout << "Finished new image at " << outpath << " " << i << "/" << fnames.size() << "\n";

			outpath.clear();
			path.clear();
		}
	}
	else
	{
		cout << "An error occured while making " << outputDirName << " directory";
		exit(1);
	}
}

void utility::Smooth(image &src, image &tgt, int SmoothSize) 
{
	cout << "Smoothing, 2D. Size: " << SmoothSize << "\n";
	tgt.resize(src.Width(), src.Height());

	int td_ = tgt.Width() / 10;
	int PercentDone = 0;

	for (int i = 0; i < tgt.Width(); i++) 
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			tgt.setPixel(i, j, RED, src.Average(i, j, RED, SmoothSize));

			if (tgt.isPPM)
			{
				tgt.setPixel(i, j, GREEN, src.Average(i, j, GREEN, SmoothSize));
				tgt.setPixel(i, j, BLUE, src.Average(i, j, BLUE, SmoothSize));
			}
		}

		if (i % td_ == 0)
		{
			cout << PercentDone << "%..";
			PercentDone += 10;
		}
	}
}

void utility::SeperableSmooth(image &src, image &tgt, int SmoothSize)
{
	cout << "Seperable Smoothing, 1D. Size: " << SmoothSize << "\n";
	tgt.resize(src.Width(), src.Height());

	if (SmoothSize == 1)
	{
		tgt.copyImage(src);
		return;
	}

	/*
		1/ss 1/ss 1/ss
		1/ss 1/ss 1/ss    =>
		1/ss 1/ss 1/ss

		1/s    
		1/s   *   1/s 1/s 1/s
		1/s
	*/

	//clamp to odd
	SmoothSize = SmoothSize % 2 == 0 ? SmoothSize + 1 : SmoothSize;

	int td_ = tgt.Width() / 5;
	int PercentDone = 0;

	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			tgt.setPixel(i, j, RED, src.getPixelAvg(i,j, 1, SmoothSize, RED));
			if (tgt.isPPM)
			{
				tgt.setPixel(i, j, GREEN, src.getPixelAvg(i, j, 1, SmoothSize, GREEN));
				tgt.setPixel(i, j, BLUE, src.getPixelAvg(i, j, 1, SmoothSize, BLUE));
			}
		}

		if (i % td_ == 0)
		{
			cout << PercentDone << "%..";
			PercentDone += 10;
		}
	}

	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			tgt.setPixel(i, j, RED, tgt.getPixelAvg(i, j, SmoothSize, 1, RED));
			if (tgt.isPPM)
			{
				tgt.setPixel(i, j, GREEN, tgt.getPixelAvg(i, j, SmoothSize, 1, GREEN));
				tgt.setPixel(i, j, BLUE, tgt.getPixelAvg(i, j, SmoothSize, 1, BLUE));
			}
		}

		if (i % td_ == 0 && i != 0)
		{
			cout << PercentDone << "%..";
			PercentDone += 10;
		}
	}
}

void utility::IncrementalSmooth(image &src, image &tgt, int SmoothSize)
{
	cout << "Incremental Smoothing, Size: " << SmoothSize << "\n";
	tgt.resize(src.Width(), src.Height());

	//clamp to odd
	SmoothSize = SmoothSize % 2 == 0 ? SmoothSize + 1 : SmoothSize;

	//variables used in calculation
	int prevSumR = 0, prevSumG = 0, prevSumB = 0;
	int SmoothOvr2 = SmoothSize / 2;

	//variables used to display progress
	int td_ = tgt.Width() / 5;
	int PercentDone = 0;

	if (SmoothSize == 1)
	{
		tgt.copyImage(src);
		return;
	}

	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			//length = number of pixels being summed on this iteration. This value changes
			//depending on if the window fits inside the image
			int length = j <= SmoothOvr2 ? SmoothOvr2 + j + 1: j >= tgt.Height() - SmoothOvr2 ?
				tgt.Height() - j + SmoothOvr2 : SmoothSize;
			float norm = 1.0f / (float)length;
			//if j == 0, get the new sum
			if (j == 0)
			{
				prevSumR = src.getPixelSum(i, j, 1, SmoothSize, RED);
				prevSumG = src.getPixelSum(i, j, 1, SmoothSize, GREEN);
				prevSumB = src.getPixelSum(i, j, 1, SmoothSize, BLUE);
			}
			//otherwise use iterative
			else
			{
				//if length < Smoothsize, then we are near an edge, and are using fewer pixels
				//to smooth the image
				if (length < SmoothSize || j <= SmoothOvr2)
				{
					//top of the image, area being examined is shrinking
					if (j > tgt.Height() / 2)
					{
						prevSumR -= src.getPixel(i, j - SmoothOvr2, RED);
						prevSumG -= src.getPixel(i, j - SmoothOvr2, GREEN);
						prevSumB -= src.getPixel(i, j - SmoothOvr2, BLUE);
					}
					//bottom of the image, area being examined is expanding
					else
					{
						//norm = 1.0f / (length + 1);
						prevSumR += src.getPixel(i, j + SmoothOvr2, RED);
						prevSumG += src.getPixel(i, j + SmoothOvr2, GREEN);
						prevSumB += src.getPixel(i, j + SmoothOvr2, BLUE);
					}
				}
				//Normal incrementation
				else
				{
					prevSumR -= src.getPixel(i, j - SmoothOvr2, RED);
					prevSumR += src.getPixel(i, j + SmoothOvr2, RED);
					prevSumG -= src.getPixel(i, j - SmoothOvr2, GREEN);
					prevSumG += src.getPixel(i, j + SmoothOvr2, GREEN);
					prevSumB -= src.getPixel(i, j - SmoothOvr2, BLUE);
					prevSumB += src.getPixel(i, j + SmoothOvr2, BLUE);
				}
			}

			tgt.setPixel(i, j, RED, (int)(norm * prevSumR));
			tgt.setPixel(i, j, GREEN, (int)(norm * prevSumG));
			tgt.setPixel(i, j, BLUE, (int)(norm * prevSumB));
		}

		if (i % td_ == 0)
		{
			cout << PercentDone << "%..";
			PercentDone += 10;
		}
	}

	prevSumR = 0; prevSumG = 0; prevSumB = 0;

	for (int i = 0; i < tgt.Height(); i++)
	{
		for (int j = 0; j < tgt.Width(); j++)
		{
			//length = number of pixels being summed on this iteration. This value changes
			//depending on if the window fits inside the image
			int length = j <= SmoothOvr2 ? SmoothOvr2 + j + 1: j >= tgt.Width() - SmoothOvr2 ?
				tgt.Width() - j + SmoothOvr2 : SmoothSize;
			float norm = 1.0f / length;
			//if j == 0, get the new sum
			if (j == 0)
			{
				prevSumR = tgt.getPixelSum(j, i, SmoothSize, 1, RED);
				prevSumG = tgt.getPixelSum(j, i, SmoothSize, 1, GREEN);
				prevSumB = tgt.getPixelSum(j, i, SmoothSize, 1, BLUE);
			}
			//otherwise use iterative
			else
			{
				//if length < Smoothsize, then we are near an edge, and are using fewer pixels
				//to smooth the image
				if (length < SmoothSize || j <= SmoothOvr2)
				{
					//top of the image, area being examined is shrinking
					if (j > tgt.Width() / 2)
					{
						
						prevSumR -= tgt.getPixel(j - SmoothOvr2, i, RED);
						prevSumG -= tgt.getPixel(j - SmoothOvr2, i, GREEN);
						prevSumB -= tgt.getPixel(j - SmoothOvr2, i, BLUE);
					}
					//bottom of the image, area being examined is expanding
					else
					{
						//norm = 1.0f / (length + 1);
						prevSumR += tgt.getPixel(j + SmoothOvr2, i, RED);
						prevSumG += tgt.getPixel(j + SmoothOvr2, i, GREEN);
						prevSumB += tgt.getPixel(j + SmoothOvr2, i, BLUE);
					}
				}
				//Normal incrementation
				else
				{
					prevSumR -= tgt.getPixel(j - SmoothOvr2, i, RED);
					prevSumR += tgt.getPixel(j + SmoothOvr2, i, RED);
					prevSumG -= tgt.getPixel(j - SmoothOvr2, i, GREEN);
					prevSumG += tgt.getPixel(j + SmoothOvr2, i, GREEN);
					prevSumB -= tgt.getPixel(j - SmoothOvr2, i, BLUE);
					prevSumB += tgt.getPixel(j + SmoothOvr2, i, BLUE);
				}
			}

			tgt.setPixel(j,i, RED, (int)(norm * prevSumR));
			tgt.setPixel(j,i, GREEN, (int)(norm * prevSumG));
			tgt.setPixel(j,i, BLUE, (int)(norm * prevSumB));
		}

		if (i % td_ == 0 && i != 0)
		{
			cout << PercentDone << "%..";
			PercentDone += 10;
		}
	}
}

void utility::IncrementalSmooth(image &src, image &tgt, int SmoothSize, vector<ROI> &roi)
{
	IncrementalSmooth(src, tgt, SmoothSize);
	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			if (!ROI::PointInsideAnyROI(i, j, roi))
			{
				tgt.setPixel(i, j, RED, src.getPixel(i, j, RED));
				if (tgt.isPPM) {
					tgt.setPixel(i, j, GREEN, src.getPixel(i, j, GREEN));tgt.setPixel(i, j, BLUE, src.getPixel(i, j, BLUE));
				}
			}
		}
	}
}
void utility::SeperableSmooth(image &src, image &tgt, int SmoothSize, vector<ROI> &roi)
{
	SeperableSmooth(src, tgt, SmoothSize);
	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			if (!ROI::PointInsideAnyROI(i, j, roi))
			{
				tgt.setPixel(i, j, RED, src.getPixel(i, j, RED));
				if (tgt.isPPM) {
					tgt.setPixel(i, j, GREEN, src.getPixel(i, j, GREEN));tgt.setPixel(i, j, BLUE, src.getPixel(i, j, BLUE));
				}
			}
		}
	}
}
void utility::Smooth(image &src, image &tgt, int SmoothSize, vector<ROI> &roi)
{
	cout << "Smoothing, 2D. Size: " << SmoothSize << "\n";
	tgt.resize(src.Width(), src.Height());

	int td_ = tgt.Width() / 10;
	int PercentDone = 0;

	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			if (ROI::PointInsideAnyROI(i, j, roi)) {
				tgt.setPixel(i, j, RED, src.Average(i, j, RED, SmoothSize));

				if (tgt.isPPM)
				{
					tgt.setPixel(i, j, GREEN, src.Average(i, j, GREEN, SmoothSize));
					tgt.setPixel(i, j, BLUE, src.Average(i, j, BLUE, SmoothSize));
				}
			}
			else
			{
				tgt.setPixel(i, j, RED, src.getPixel(i, j, RED));
				if (tgt.isPPM) {
					tgt.setPixel(i, j, GREEN, src.getPixel(i, j, GREEN));tgt.setPixel(i, j, BLUE, src.getPixel(i, j, BLUE));
				}
			}
		}

		if (i % td_ == 0)
		{
			cout << PercentDone << "%..";
			PercentDone += 10;
		}
	}
}
void utility::ColorBinarize(image &src, image &tgt, RGB color, int distance, vector<ROI> &roi)
{
	if (!src.isPPM) {
		cout << "Error, ColorBinarize is only for colored images!\n";
		exit(1);
	}

	tgt.resize(src.Width(), src.Height());
	RGB black(0, 0, 0); RGB white(255, 255, 255);

	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			RGB srcColor = src.getColorVector(i, j);
			if (ROI::PointInsideAnyROI(i, j, roi)) {
				if (RGB::distance(srcColor, color) > distance)
				{
					tgt.setColorVector(i, j, black);
				}
				else
				{
					tgt.setColorVector(i, j, white);
				}
			}
			else {
				tgt.setColorVector(i,j,srcColor);
			}
		}
	}
}
void utility::Threshold(image &src, image &tgt, int value, vector<ROI> &roi)
{
	tgt.resize(src.Width(), src.Height());
	for (int i = 0; i<src.Width(); i++)
		for (int j = 0; j < src.Height(); j++)
		{
			if (ROI::PointInsideAnyROI(i, j, roi))
			{
				if (src.getPixel(i, j) >= value)
				{
					//set white
					tgt.setPixel(i, j, RED, 255);
					if (src.isPPM) { tgt.setPixel(i, j, GREEN, 255);tgt.setPixel(i, j, BLUE, 255); }
				}
				else
				{
					//set black
					tgt.setPixel(i, j, RED, 0);
					if (src.isPPM) { tgt.setPixel(i, j, GREEN, 0);tgt.setPixel(i, j, BLUE, 0); }
				}
			}
			else
			{
				tgt.setPixel(i, j, RED, src.getPixel(i, j, RED));
				if (tgt.isPPM) {
					tgt.setPixel(i, j, GREEN, src.getPixel(i, j, GREEN));tgt.setPixel(i, j, BLUE, src.getPixel(i, j, BLUE));
				}
			}
		}
}

void utility::MakeHistograms(image &src, string imgName, int numberDivisions, int rgb, int hsi)
{
	size_t pos = imgName.find(".ppm");
	size_t pos1 = imgName.find(".pgm");

	if (imgName.find(".ppm") != std::string::npos)
	{
		imgName = imgName.substr(0, pos);
	}
	else
	{
		imgName = imgName.substr(0, pos1);
	}

	//whole image
	if (numberDivisions == 0)
	{
		vector<float> v = *src.getPMF(0, 0, src.Width(), src.Height(), rgb, hsi);
		string fname(imgName); fname.append("_histogram"); 
		if (imgName.find(".ppm") != std::string::npos) fname.append(".ppm"); else fname.append(".pgm");
		OutPutHistogram(fname, v);
	}
	//quarters
	else
	{
		int rowlen = src.Width() / 2, collen = src.Height() / 2;
		vector<float> v = *src.getPMF(0, 0, rowlen, collen, rgb, hsi);
		string fname(imgName); fname.append("_histogram_topleft");
		if (imgName.find(".ppm") != std::string::npos) fname.append(".ppm"); else fname.append(".pgm");
		OutPutHistogram(fname, v);

		vector<float> v1 = *src.getPMF(0, collen, rowlen, collen, rgb, hsi);
		string fname1(imgName); fname1.append("_histogram_topright");
		if (imgName.find(".ppm") != std::string::npos) fname1.append(".ppm"); else fname1.append(".pgm");
		OutPutHistogram(fname1, v1);

		vector<float> v2 = *src.getPMF(rowlen, 0, rowlen, collen, rgb, hsi);
		string fname2(imgName); fname2.append("_histogram_bottomleft");
		if (imgName.find(".ppm") != std::string::npos) fname2.append(".ppm"); else fname2.append(".pgm");
		OutPutHistogram(fname2, v2);

		vector<float> v3 = *src.getPMF(rowlen, collen, rowlen, collen, rgb, hsi);
		string fname3(imgName); fname3.append("_histogram_bottomright");
		if (imgName.find(".ppm") != std::string::npos) fname3.append(".ppm"); else fname3.append(".pgm");
		OutPutHistogram(fname3, v3);
	}
}
void utility::HistogramEqualization(image &src, image &tgt, int rgb, int hsi, int numDivisions)
{
	if (tgt.Height() != src.Height() || src.Width() != tgt.Width())
		tgt.resize(src.Width(), src.Height());

	if (numDivisions == 0)
	{
		Equalize(src, tgt, rgb, hsi, 0, 0, tgt.Width(), tgt.Height());
	}
	else
	{
		Equalize(src, tgt, rgb, hsi, 0, 0, tgt.Width() / 2, tgt.Height() / 2);
		Equalize(src, tgt, rgb, hsi, tgt.Width() / 2, 0, tgt.Width() / 2, tgt.Height() / 2);
		Equalize(src, tgt, rgb, hsi, 0, tgt.Height() / 2, tgt.Width() / 2, tgt.Height() / 2);
		Equalize(src, tgt, rgb, hsi, tgt.Width() / 2, tgt.Height() / 2,
			tgt.Width() / 2, tgt.Height() / 2);
	}
}
void utility::CopyEmptyChannels(image &src, image &tgt)
{
	if (tgt.Height() != src.Height() || src.Width() != tgt.Width())
		tgt.resize(src.Width(), src.Height());

	bool r = false, g = false, b = false;
	if (tgt.ChannelIsEmpty(RED))
		r = true;
	if (tgt.isPPM && tgt.ChannelIsEmpty(GREEN))
		g = true;
	if (tgt.isPPM && tgt.ChannelIsEmpty(BLUE))
		b = true;

	for (int i = 0; i < src.Width(); i++)
	{
		for (int j = 0; j < src.Height(); j++)
		{
			if (r == true)
				tgt.setPixel(i, j, RED, src.getPixel(i, j, RED));
			if (g == true && tgt.isPPM)
				tgt.setPixel(i, j, GREEN, src.getPixel(i, j, GREEN));
			if (b == true && tgt.isPPM)
				tgt.setPixel(i, j, BLUE, src.getPixel(i, j, BLUE));
		}
	}
}

void utility::AddHSI(image &src, image &tgt, HSI value)
{
	tgt.resize(src.Height(), src.Width());
	cout << value.h << ", " << value.s << ", " << value.i;
	for (int i = 0; i < src.Width(); i++)
	{
		for (int j = 0; j < src.Height(); j++)
		{
			//get
			HSI hsi = src.getColor(i, j);

			//modify values
			hsi.i += value.i;
			hsi.h += value.h;
			hsi.s += value.s;

			//clamp
			if (hsi.i < 0) hsi.i = 0.0f; if (hsi.i > 1) hsi.i = 1.0f;
			if (hsi.s < 0) hsi.s = 0.0f; if (hsi.s > 1) hsi.s = 1.0f;
			if (hsi.h < 0) hsi.h = 0.0f; if (hsi.h > 360) hsi.h = 360.0f;

			//set
			tgt.setColor(i, j, hsi);
		}
	}
}

void Equalize(image &src, image &tgt, int rgb, int hsi, int row, int col, int rowlen, int collen)
{
	vector<float> pmf = *src.getPMF(row, col, rowlen, collen, rgb, hsi);
	vector<float> cdf; cdf.resize(pmf.size());
	vector<int> equalizedValues; equalizedValues.resize(cdf.size());

	//build cdf function
	for (size_t i = 0; i < cdf.size(); i++)
	{
		if (i != 0)
			cdf[i] = pmf[i] + cdf[i - 1];
		else
			cdf[i] = pmf[i];
	}

	//build equalized values mapping
	for (size_t i = 0; i < equalizedValues.size(); i++)
	{
		equalizedValues[i] = (int)((equalizedValues.size() - 1) * cdf[i]);
	}

	//bounds of iteration
	int rowstop = row + rowlen; rowstop = rowstop > src.Width() ? src.Width() : rowstop;
	int colstop = col + collen; colstop = colstop > src.Height() ? src.Height() : colstop;

	//apply histogram
	if (rgb <= 2 && rgb >= 0)
	{
		//rgb histogram values
		for (int i = row; i < rowstop; i++)
		{
			for (int j = col; j < colstop; j++)
			{
				int value = src.getPixel(i, j, rgb);
				int newValue = equalizedValues[value];
				tgt.setPixel(i, j, rgb, newValue);
			}
		}
	}
	else if (hsi <= 2 && hsi >= 0)
	{
		//doing hsi values
		for (int i = row; i < rowstop; i++)
		{
			for (int j = col; j < colstop; j++)
			{
				HSI value = src.getColor(i, j);
				if (hsi == SATURATION)
				{
					value.s = equalizedValues[(int)(value.s * 359)] / 360.0f;
					tgt.setColor(i, j, value);
				}
				else if (hsi == INTENSITY)
				{
					value.i = equalizedValues[(int)(value.i * 359)] / 360.0f;
					tgt.setColor(i, j, value);
				}
				else
				{
					value.h = (float)equalizedValues[(int)value.h];
					tgt.setColor(i, j, value);
				}
			}
		}
	}
}

void utility::Stretch(image &src, image &tgt)
{
	if (src.isPPM)
	{
		cout << "Stretching is only required for grey level images!\n";
		exit(0);
	}

	if (tgt.Height() != src.Height() || src.Width() != tgt.Width())
		tgt.resize(src.Width(), src.Height());

	int max = 0, min = 255;
	//find max/min
	for (int i = 0; i < src.Width(); i++)
	{
		for (int j = 0; j < src.Height(); j++)
		{
			int value = src.getPixel(i, j);
			if (value > max)
				max = value;
			if (value < min)
				min = value;
		}
	}

	//apply stretching
	for (int i = 0; i < src.Width(); i++)
	{
		for (int j = 0; j < src.Height(); j++)
		{
			float value = (float)src.getPixel(i, j);
			value = ((value - min) / (max - min)) * 255;
			tgt.setPixel(i, j, (int)value);
		}
	}
}

void utility::EdgeDetect(image &src, image &tgt, image &t, image &dir, vector<ROI> &roi,
	int threshold, int dir_threshold, int rgb, int hsi, bool _5x5)
{
	int filterx[5][5] = 
	{
		{ -5,   -4, 0,  4,  5 },
		{ -8,  -10, 0, 10,  8 },
		{ -10, -20, 0, 20, 10 },
		{ -8,  -10, 0, 10,  8 },
		{ -5,   -4, 0,  4,  5 },
	};
	int filtery[5][5] =
	{
		{  4,  10,  20,  10,  4 },
		{  5,   8,  10,   8,  5 },
		{  0,   0,   0,   0,  0 },
		{ -4, -10, -20, -10, -4 },
		{ -5,  -8, -10,  -8, -5 }
	};
	int filterx_3x3[3][3] = 
	{
		{ -1, 0, 1 },
		{ -2, 0, 2 },
		{ -1, 0, 1 }
	};
	int filtery_3x3[3][3] =
	{
		{  1,  2,  1 },
		{  0,  0,  0 },
		{ -1, -2, -1 }
	};

	tgt.resize(src.Width(), src.Height());
	t.resize(src.Width(), src.Height());
	dir.resize(src.Width(), src.Height());

	for (int i = 0; i < tgt.Width(); i++)
	{
		for (int j = 0; j < tgt.Height(); j++)
		{
			if (_5x5) 
			{
				//if this pixel is inside the region and not a border pixel...
				if ((roi.size() == 0 || ROI::PointInsideAnyROI(i,j,roi)) && tgt.filter_fits(5, 5, i, j))
				{
					float dx = src.filter_abs_5x5(filterx, i, j, 5, 5, rgb, hsi);
					float dy = src.filter_abs_5x5(filtery, i, j, 5, 5, rgb, hsi);

					float g = sqrtf(dx * dx + dy * dy);

					float theta = atan2f(dy , dx);
					theta = HSI::rad2Deg(theta);
					if (theta < 0) theta += 360.0f;

					if (rgb != -1) 
					{
						int value = (int)(g * 255.0f);
						//set gradient
						tgt.setPixel(i, j, rgb, value);

						//threshold gradient
						if (value > threshold)
							t.setPixel(i, j, rgb, 255);

						//threshold gradient and direction
						if (value > threshold)
						{
							int thetai = (int)theta; //do a direction opposite the current one (0 and 180 degrees both make horizontal lines, etc)
							int d1 = dir_threshold; int d2 = 180 + dir_threshold; if (d2 >= 360) d2 -= 360;
							if (abs(thetai - d1) <= 10 || abs(thetai - d1) >= 350)
								dir.setPixel(i, j, rgb, 255);
							if (abs(thetai - d2) <= 10 || abs(thetai - d2) >= 350)
								dir.setPixel(i, j, rgb, 255);
						}
					}
					else
					{
						HSI current = src.getColor(i, j);

						int value = (int)(g * 360.0f);
						HSI white(0, 0, 1);
						HSI hsi_val; hsi_val.h = 0; hsi_val.i = 0; hsi_val.s = 0;

						if (hsi == HUE) { hsi_val.i = g; hsi_val.h = g * 360.0f; hsi_val.s = 1.0f; }
						else if (hsi == SATURATION) { hsi_val.s = g;  hsi_val.i = g;}
						else hsi_val.i = g;

						//set gradient
						tgt.setColor(i, j, hsi_val);

						//threshold gradient
						if (value > threshold)
							t.setColor(i, j, white);

						//threshold gradient and direction
						if (value > threshold)
						{
							int thetai = (int)theta;
							int d1 = dir_threshold; int d2 = 180 + dir_threshold; if (d2 >= 360) d2 -= 360;
							if (abs(thetai - d1) <= 10 || abs(thetai - d1) >= 350)
								dir.setColor(i, j, white);
							if (abs(thetai - d2) <= 10 || abs(thetai - d2) >= 350)
								dir.setColor(i, j, white);
						}
					}
				}
				//fill black or copy depending if ROI is enabled
				else
				{
					if (roi.size() > 0)
					{
						if (rgb != -1)
						{
							tgt.setColorVector(i, j, src.getColorVector(i, j));
							t.setColorVector(i, j, src.getColorVector(i, j));
							dir.setColorVector(i, j, src.getColorVector(i, j));
						}
						else
						{
							tgt.setColor(i, j, src.getColor(i, j));
							t.setColor(i, j, src.getColor(i, j));
							dir.setColor(i, j, src.getColor(i, j));
						}
					}
					else
					{
						if (rgb != -1)
						{
							tgt.setPixel(i, j, rgb, 0);
							t.setPixel(i, j, rgb, 0);
							dir.setPixel(i, j, rgb, 0);
						}
						else
						{
							tgt.setColor(i, j, HSI(0, 0, 0));
							t.setColor(i, j, HSI(0, 0, 0));
							dir.setColor(i, j, HSI(0, 0, 0));
						}
					}
				}
			}
			else
			{
				if ((roi.size() == 0 || ROI::PointInsideAnyROI(i, j, roi)) && tgt.filter_fits(3, 3, i, j))
				{
					float dx = src.filter_abs_3x3(filterx_3x3, i, j, 3, 3, rgb, hsi);
					float dy = src.filter_abs_3x3(filtery_3x3, i, j, 3, 3, rgb, hsi);

					float g = sqrtf(dx * dx + dy * dy);

					float theta = atan2f(dy, dx);
					theta = HSI::rad2Deg(theta);
					if (theta < 0) theta += 360.0f;

					if (rgb != -1)
					{
						int value = (int)(g * 255.0f);
						//set gradient
						tgt.setPixel(i, j, rgb, value);

						//threshold gradient
						if (value > threshold)
							t.setPixel(i, j, rgb, 255);

						//threshold gradient and direction
						if (value > threshold)
						{
							int thetai = (int)theta;
							int d1 = dir_threshold; int d2 = 180 + dir_threshold; if (d2 >= 360) d2 -= 360;
							if (abs(thetai - d1) <= 10 || abs(thetai - d1) >= 350)
								dir.setPixel(i, j, rgb, 255);
							if (abs(thetai - d2) <= 10 || abs(thetai - d2) >= 350)
								dir.setPixel(i, j, rgb, 255);
						}
					}
					else
					{
						HSI current = src.getColor(i, j);

						int value = (int)(g * 360.0f);
						HSI white(0, 0, 1);
						HSI hsi_val; hsi_val.h = 0; hsi_val.i = 0; hsi_val.s = 0;

						if (hsi == HUE) { hsi_val.i = g; hsi_val.h = g * 360.0f; hsi_val.s = 1.0f;}
						else if (hsi == SATURATION) { hsi_val.s = g;  hsi_val.i = g; }
						else hsi_val.i = g;

						//set gradient
						tgt.setColor(i, j, hsi_val);

						//threshold gradient
						if (value > threshold)
							t.setColor(i, j, white);

						//threshold gradient and direction
						if (value > threshold)
						{
							int thetai = (int)theta;

							int d1 = dir_threshold; int d2 = 180 + dir_threshold; if (d2 >= 360) d2 -= 360;
							if (abs(thetai - d1) <= 10 || abs(thetai - d1) >= 350)
								dir.setColor(i, j, white);
							if (abs(thetai - d2) <= 10 || abs(thetai - d2) >= 350)
								dir.setColor(i, j, white);
						}
					}
				}
				else
				{
					if (roi.size() > 0)
					{
						if (rgb != -1) 
						{
							tgt.setColorVector(i, j, src.getColorVector(i, j));
							t.setColorVector(i, j, src.getColorVector(i, j));
							dir.setColorVector(i, j, src.getColorVector(i, j));
						}
						else 
						{
							tgt.setColor(i, j, src.getColor(i, j));
							t.setColor(i, j, src.getColor(i, j));
							dir.setColor(i, j, src.getColor(i, j));
						}
					}
					else
					{
						if (rgb != -1)
						{
							tgt.setPixel(i, j, rgb, 0);
							t.setPixel(i, j, rgb, 0);
							dir.setPixel(i, j, rgb, 0);
						}
						else 
						{
							tgt.setColor(i, j, HSI(0, 0, 0));
							t.setColor(i, j, HSI(0, 0, 0));
							dir.setColor(i, j, HSI(0, 0, 0));
						}
					}
				}
			}
		}
	}
}

void OutPutHistogram(string filename, vector<float> &pmf)
{
	image histo;
	histo.isPPM = false;
	histo.resize((int)pmf.size(), (int)pmf.size());

	//find maximum value
	float max = 0.0f; float multiplier = 0.0f;
	for (size_t i = 0; i < pmf.size(); i++)
	{
		if (pmf[i] > max)
			max = pmf[i];
	}

	//find normalizing value for display
	multiplier = pmf.size() / max;

	//fill image
	for (size_t i = 0; i < pmf.size(); i++)
	{
		int height = (int)(multiplier * pmf[i]);
		if (height > 0)
			for (size_t j = pmf.size() - height; j < pmf.size(); j++)
			{
				histo.setPixel((int)i, (int)j, 255);
			}
	}

	histo.save(filename.c_str());
}

void ListAllFilesInDir(const char *directory, vector<string> &fnames) 
{
	string s(directory);
	string s1(directory);
	s1.append("\\*.pgm");
	s.append("\\*.ppm");

	char * directory1 = &s[0];
	char * directory2 = &s1[0];

	WIN32_FIND_DATAA file_data;

	HANDLE hFind = INVALID_HANDLE_VALUE;

	if ((hFind = FindFirstFileA(directory1, &file_data)) == INVALID_HANDLE_VALUE) {

		cout << "Couldn't find any ppm files in directory "<< directory << "\n";
	}
	else 
	{
		fnames.push_back(file_data.cFileName);
		while (FindNextFileA(hFind, &file_data)) {

			fnames.push_back(file_data.cFileName);
		}
	}

	FindClose(hFind);

	WIN32_FIND_DATAA file_data1;

	HANDLE hFind1 = INVALID_HANDLE_VALUE;

	if ((hFind1 = FindFirstFileA(directory2, &file_data1)) == INVALID_HANDLE_VALUE) {

		cout << "Couldn't find any pgm files in directory " << directory << "\n";
	}
	else
	{
		fnames.push_back(file_data1.cFileName);

		while (FindNextFileA(hFind1, &file_data1)) {

			fnames.push_back(file_data1.cFileName);

		}
	}

	FindClose(hFind1);
}
float Pow2(int y) 
{
	if (y == 0)
		return 1;
	if (y > 0) 
	{
		float val = 1;
		//scaling by factor of 2^y
		for (int i = 0; i < y; i++)
			val *= 2;
		return val;
	}
	else
	{
		float val = 1;
		//scaling by factor of 2^y
		for (int i = 0; i < abs(y); i++)
			val = val / (float)2;
		return val;
	}
}
//directory exists function inspired by http://stackoverflow.com/questions/8233842/how-to-check-if-directory-exist-using-c-and-winapi
bool dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}
