
#include "stdafx.h"
#include "image.h"
#include <string>
#include <iostream>     
#include <fstream>     

using namespace std;

RGB::RGB() {
	r = 0;
	b = 0;
	g = 0;
}

RGB::RGB(int r_, int g_, int b_) 
{
	r = r_; g = g_; b = b_;
}

HSI::HSI() {
	h = 0;
	s = 0;
	i = 0;
}

HSI::HSI(float h_, float s_, float i_)
{
	h = h_; s = s_; i = i_;
}

image::image()
{
	data.width = data.height = 0;
	data.redChannel.clear();
	data.greenChannel.clear();
	data.blueChannel.clear();
}
/*-----------------------------------------------------------------------**/
image::image(image &img)
{
	this->copyImage(img);
	this->isPPM = img.isPPM;
}

/*-----------------------------------------------------------------------**/
image::~image()
{
	this->deleteImage();
}

/*-----------------------------------------------------------------------**/
bool image::isInbounds(int i, int j)
{
	if ((i < 0) || (j < 0) || (j >= data.height) || (i >= data.width))
		return false;
	else
		return true;
}

/*-----------------------------------------------------------------------**/
void image::deleteImage()
{
	this->data.width = this->data.height= 0;
	this->data.redChannel.clear();
	this->data.greenChannel.clear();
	this->data.blueChannel.clear();
}

/*-----------------------------------------------------------------------**/
void image::copyImage(image &img)
{
	this->resize(img.Width(), img.Height());

	for (int i = 0; i < img.Width(); i++)
	{
		for (int j = 0; j < img.Height(); j++)
		{
			setPixel(i, j, RED, img.getPixel(i, j, RED));
			if (isPPM)
			{
				setPixel(i, j, GREEN, img.getPixel(i, j, GREEN));
				setPixel(i, j, BLUE, img.getPixel(i, j, BLUE));
			}
		}
	}
}


/*-----------------------------------------------------------------------**/
void image::resize(int width, int height)
{
	data.width = width;
	data.height = height;
	data.redChannel.clear();
	data.greenChannel.clear();
	data.blueChannel.clear();

	data.redChannel.resize(width);
	data.greenChannel.resize(width);
	data.blueChannel.resize(width);
	for (int i = 0; i < width; i++)
	{
		data.redChannel[i].resize(height);
		data.greenChannel[i].resize(height);
		data.blueChannel[i].resize(height);
	}

}

/*----------------------------------------------------------------------*/
void image::setPixels(const int x, const int y, const int rgb, const int width, const int height, const int value)
{
	int stoprow = width + x; int stopcol = y + height;
	for (int i = x; i < stoprow && i < Width(); i++)
		for (int j = y; j < stopcol && j < Height(); j++)
		{
			setPixel(i, j, rgb, value);
		}
}

/*------------------------------------------------------------------------*/
int image::Average(const int x, const int y, const int rgb, int width)
{
	//make width odd if it isnt
	width = width % 2 == 0 ? width + 1 : width;
	float total = 0;
	int num_pixels = 0;

	int rowstart, colstart, rowstop, colstop;

	//define where the averaging will take place, clamp the values as necessary
	rowstart = x - width / 2; rowstart = rowstart < 0 ? 0 : rowstart;
	colstart = y - width / 2; colstart = colstart < 0 ? 0 : colstart;

	rowstop = x + width / 2; rowstop = rowstop >= Width() ? Width() - 1 : rowstop;
	colstop = y + width / 2; colstop = colstop >= Height() ? Height() - 1 : colstop;

	for (int i = rowstart; i <= rowstop; i++)
		for (int j = colstart; j <= colstop; j++)
		{
			total += getPixel(i, j, rgb);
			num_pixels++;
		}

	return num_pixels == 0 ? 0 : (int)(((float)total) / ((float)num_pixels));
}

/*-------------------------------------------------------------------*/
void image::setPixel(const int i, const int j, const int value)
{
	data.redChannel[i][j] = value;
}

int image::getPixelAvg(const int ix, const int iy, const int x, const int y, int rgb) 
{
	int total = 0;
	int rowstart, colstart, rowstop, colstop;
	int numPixels = 0;

	//define where the averaging will take place, clamp the values as necessary
	rowstart = ix - x / 2; rowstart = rowstart < 0 ? 0 : rowstart;
	colstart = iy - y / 2; colstart = colstart < 0 ? 0 : colstart;

	rowstop = ix + x / 2; rowstop = rowstop >= Width() ? Width() - 1 : rowstop;
	colstop = iy + y / 2; colstop = colstop >= Height() ? Height() - 1 : colstop;

	for (int i = rowstart; i <= rowstop; i++)
		for (int j = colstart; j <= colstop; j++)
		{
			total += getPixel(i, j, rgb);
			numPixels++;
		}

	return numPixels == 0 ? 0 : total / numPixels;
}

int image::getPixelSum(const int ix, const int iy, const int x, const int y, int rgb)
{
	//Note, this function may not sum the entire size of the input rectangle
	int total = 0;
	int rowstart, colstart, rowstop, colstop;

	//define where the averaging will take place, clamp the values as necessary
	rowstart = ix - x / 2; rowstart = rowstart < 0 ? 0 : rowstart;
	colstart = iy - y / 2; colstart = colstart < 0 ? 0 : colstart;

	rowstop = ix + x / 2; rowstop = rowstop >= Width() ? Width() - 1 : rowstop;
	colstop = iy + y / 2; colstop = colstop >= Height() ? Height() - 1 : colstop;

	for (int i = rowstart; i <= rowstop; i++)
		for (int j = colstart; j <= colstop; j++)
		{
			total += getPixel(i, j, rgb);
		}

	return total;
}

/*-------------------------------------------------------------------*/
void image::setPixel(const int i, const int j, const int rgb, const int value)
{
	switch (rgb)
	{
	case RED: data.redChannel[i][j] = value; break;
	case GREEN: data.greenChannel[i][j] = value; break;
	case BLUE: data.blueChannel[i][j] = value; break;
	}
}

/*-------------------------------------------------------------------*/
int image::getPixel(const int i, const int j)
{
	return data.redChannel[i][j];
}

/*-------------------------------------------------------------------*/
int image::getPixel(const int i, const int j, const int rgb)
{
	int value;
	switch (rgb)
	{
	case RED: value = data.redChannel[i][j]; break;
	case GREEN: value = data.greenChannel[i][j]; break;
	case BLUE: value = data.blueChannel[i][j]; break;
	}
	return value;
}

/*-------------------------------------------------------------------*/
HSI image::getColor(const int i, const int j)
{
	HSI color;
	RGB c = getColorVector(i, j);

	float r = c.r / 255.0f, g = c.g / 255.0f, b = c.b / 255.0f;

	float numerator = (r - g) + (r - b);
	float denominator =  (float)sqrt((r - g) * (r - g) + (r - b) * (g - b));
	
	float theta = acos(0.5f * (numerator / denominator));
	theta = HSI::rad2Deg(theta);
	//if (col % 100 == 0)
	//	cout << "|c"<< numerator << " " << denominator << " " << theta << " ";
	color.h = b <= g ? theta : 360 - theta;

	float intensity = (r + g + b) / 3.0f;
	float min = r < b ? (r < g ? r : g) : (b < g ? b : g);
	color.s = intensity == 0 ? 0 : 1 - min / intensity;

	color.i = intensity;

	return color;
}

void image::setColor(const int ix, const int iy, const HSI value)
{
	RGB color;

	float i = value.i;
	float s = value.s;
	float h = value.h;

	float r, g, b;

	if (h == 0 || h == 360)
	{
		r = i + 2 * i * s;
		g = i - i * s;
		b = i - i * s;
	}
	else if (h > 0 && h < 120)
	{
		float d60 = HSI::deg2Rad(60.0f - h);
		float d0 = HSI::deg2Rad((float)h);
		r = i + i * s * (cos(d0) / cos(d60));
		g = i + i * s * (1 - cos(d0) / cos(d60));
		b = i - i * s;
	}
	else if (h == 120)
	{
		r = i - i * s;
		g = i + 2 * i * s;
		b = i - i * s;
	}
	else if (h > 120 && h < 240)
	{
		float d180 = HSI::deg2Rad(180.0f - h);
		float d120 = HSI::deg2Rad(h - 120.0f);
		r = i - i * s;
		g = i + i * s * (cos(d120) / cos(d180));
		b = i + i * s * (1 - cos(d120) / cos(d180));
	}
	else if (h == 240)
	{
		r = i - i * s;
		g = i - i * s;
		b = i + 2 * i * s;
	}
	else if (h > 240 && h < 360)
	{
		float d300 = HSI::deg2Rad(300.0f - h);
		float d240 = HSI::deg2Rad(h - 240.0f);
		r = i + i * s * (1 - cos(d240) / cos(d300));
		g = i - i * s;
		b = i + i * s * (cos(d240) / cos(d300));
	}
	else
	{
		color = getColorVector(ix, iy);
		return;
	}
	color.r = (int)(r * 255);
	color.g = (int)(g * 255);
	color.b = (int)(b * 255);

	if (color.r > 255) color.r = 255; if (color.r < 0) color.r = 0;
	if (color.g > 255) color.g = 255; if (color.g < 0) color.g = 0;
	if (color.b > 255) color.b = 255; if (color.b < 0) color.b = 0;

	setColorVector(ix, iy, color);
}

RGB image::getColorVector(const int i, const int j)
{
	RGB color;
	color.r = getPixel(i, j, RED);
	if (!isPPM) {
		color.g = 0; color.b = 0;
		return color;
	}
	color.g = getPixel(i, j, GREEN);
	color.b = getPixel(i, j, BLUE);
	return color;
}

void image::setColorVector(const int i, const int j, RGB value) 
{
	setPixel(i, j, RED, value.r);
	setPixel(i, j, GREEN, value.g);
	setPixel(i, j, BLUE, value.b);
}

float RGB::distance(RGB color1, RGB color2)
{
	float rd = (float)color2.r - (float)color1.r;
	float gd = (float)color2.g - (float)color1.g;
	float bd = (float)color2.b - (float)color1.b;
	if (rd == 0 && gd == 0 && bd == 0)
		return 0;
	return sqrt(rd * rd + gd * gd + bd * bd);
}

/*-----------------------------------------------------------------------**/
int image::Width()
{
	return data.width;
}

/*-----------------------------------------------------------------------**/
int image::Height()
{
	return data.height;
}

/*-------------------------------------------------------------------*/
bool image::save(const char* file)
{
	PPMObject o;
	o.height = data.height;
	o.width = data.width;
	o.maxColVal = 255;
	o.magicNum = isPPM ? "P6" : "P5";
	if (isPPM) 
	{
		o.m_Ptr = new char[data.width * data.height * 3];
		int n = 0;
		for (int j = 0; j < data.height; j++)
		{
			for (int i = 0; i < data.width; i++) {
				o.m_Ptr[n] = (unsigned char)data.redChannel[i][j];
				o.m_Ptr[n + 1] = (unsigned char)data.greenChannel[i][j];
				o.m_Ptr[n + 2] = (unsigned char)data.blueChannel[i][j];
				n += 3;
			}
		}
	}
	else
	{
		o.m_Ptr = new char[data.width * data.height];
		int n = 0;
		for (int j = 0; j < data.height; j++)
		{
			for (int i = 0; i < data.width; i++) 
			{
				o.m_Ptr[n] = (unsigned char)data.redChannel[i][j];
				n++;
			}
		}
	}
	std::string name(file);
	ofstream f(name.c_str(), ios::binary);
	if (f.fail())
	{
		cout << "Could not open file: " << name << endl;
		exit(1);
	}
	saveOut(f, o);
	f.close();
	return true;
}

/*-------------------------------------------------------------------*/
bool image::save(char* file)
{
	const char* name = file;
	return save(name);
}
/*-------------------------------------------------------------------*/

bool image::read(char *file)
{
	std::string name(file);

	if (name.find("ppm") == string::npos && name.find("pgm") == string::npos) 
	{
		cout << "Error, fle must be ppm or pgm format!";
		exit(1);
	}

	ifstream f(name.c_str(), ios::binary);
	if (f.fail())
	{
		cout << "Could not open file: " << name << endl;
		exit(1);
	}

	PPMObject o;
	readIn(f, o);
	data.width = o.width;
	data.height = o.height;

	resize(data.width, data.height);

	if (o.magicNum == "P6") 
	{
		isPPM = true;
		int n = 0;
		for (int j = 0; j < data.height; j++)
		{
			for (int i = 0; i < data.width; i++) 
			{
				data.redChannel[i][j] = (unsigned char)o.m_Ptr[n];
				data.greenChannel[i][j] = (unsigned char)o.m_Ptr[n + 1];
				data.blueChannel[i][j] = (unsigned char)o.m_Ptr[n + 2];
				n+=3;
			}
		}
	}
	else
	{
		isPPM = false;
		int n = 0;
		for (int j = 0; j < data.height; j++)
		{
			for (int i = 0; i < data.width; i++)
			{
				data.redChannel[i][j] = (unsigned char)o.m_Ptr[n];
				n++;
			}
		}
	}

	f.close();

	return true;
}

//small reading/saving scheme influenced by http://stackoverflow.com/questions/28896001/read-write-to-ppm-image-file-c

void image::readIn(istream &inputStream, PPMObject &other)
{
	inputStream >> other.magicNum;

	if (other.magicNum != "P6" && other.magicNum != "P5") 
	{
		cout << "File is not PGM or PPM!";
		exit(1);
	}

	eat_comment(inputStream);
	inputStream >> other.width >> other.height;
	eat_comment(inputStream);
	inputStream >> other.maxColVal;
	inputStream.get(); // skip the trailing white space
	size_t size = other.magicNum == "P6" ? other.width * other.height * 3 : other.width * other.height;
	other.m_Ptr = new char[size];
	inputStream.read(other.m_Ptr, size);
}

void image::saveOut(ostream &outputStream, const PPMObject &other)
{
	outputStream << other.magicNum << "\n"
		<< other.width << " "
		<< other.height << "\n"
		<< other.maxColVal << "\n"
		;
	size_t size = isPPM ? other.width * other.height * 3 : other.width * other.height;
	outputStream.write(other.m_Ptr, size);
}

//comment detecting for each line influenced by http://josiahmanson.com/prose/optimize_ppm/

void image::eat_comment(istream &f)
{
	char linebuf[1024];
	char ppp;
	while (ppp = f.peek(), ppp == '\n' || ppp == '\r')
		f.get();
	if (ppp == '#')
		f.getline(linebuf, 1023);
}

//get probability mass function for this image
vector<float> * image::getPMF(const int x, const int y, const int rowlen, const int collen, int rgb, int hsi)
{
	vector<int> count;
	vector<float> * pmf = new vector<float>();
	int rowstop = x + rowlen; rowstop = rowstop > Width() ? Width() : rowstop;
	int colstop = y + collen; colstop = colstop > Height() ? Height() : colstop;

	//get frequency of each value
	if (rgb <= 2 && rgb >= 0)
	{
		count.resize(256);
		pmf->resize(256);

		for (int i = x; i < rowstop; i++)
		{
			for (int j = y; j < colstop; j++)
			{
				count[getPixel(i, j, rgb)]++;
			}
		}
	}
	else if (hsi <= 2 && hsi >= 0)
	{
		count.resize(360);
		pmf->resize(360);
		for (int i = x; i < rowstop; i++)
		{
			for (int j = y; j < colstop; j++)
			{
				HSI value = getColor(i, j);
				if (hsi == INTENSITY)
				{
					count[(int)(value.i * 359)]++;
				}
				else if (hsi == SATURATION)
				{
					count[(int)(value.s * 359)]++;
				}
				else 
				{
					count[(int)value.h]++;
				}
			}
		}
	}

	//calculate image area
	float area = (float)(collen * rowlen);

	//assign % of image taken for each color
	for (size_t i = 0; i < pmf->size(); i++)
	{
		(*pmf)[i] = count[i] / area;
	}
	return pmf;
}

//returns true if the input channel is all 0 or empty
bool image::ChannelIsEmpty(int rgb)
{
	if (rgb == RED)
	{
		if (data.redChannel.size() == 0)
			return true;
		for (size_t i = 0; i < data.redChannel.size(); i++)
			for (size_t j = 0; j < data.redChannel[i].size(); j++)
				if (data.redChannel[i][j] != 0)
				return false;
		return true;
	}
	else if (!isPPM)
		return true;
	else if (rgb == GREEN)
	{
		if (data.greenChannel.size() == 0)
			return true;
		for (size_t i = 0; i < data.greenChannel.size(); i++)
			for (size_t j = 0; j < data.greenChannel[i].size(); j++)
				if (data.greenChannel[i][j] != 0)
				return false;
		return true;
	}
	else 
	{
		if (data.blueChannel.size() == 0)
			return true;
		for (size_t i = 0; i < data.blueChannel.size(); i++)
			for (size_t j = 0; j < data.blueChannel[i].size(); j++)
				if (data.blueChannel[i][j] != 0)
				return false;
		return true;
	}
}

//returns result of filter, no bounds checking. Clamps values 0 to 1
float image::filter_abs_5x5(int filter[5][5], int image_x, int image_y, int fx, int fy, int rgb, int hsi)
{
	if (fx % 2 == 0 || fy % 2 == 0)
	{
		cout << "Must use odd filter size\n";
		exit(0);
	}

	int fsum = 0;
	for (int i = 0; i < fx; i++)
	{
		for (int j = 0; j < fy; j++)
		{
			fsum += abs(filter[i][j]);
		}
	}

	int sx = image_x - fx / 2;
	int sy = image_y - fy / 2;
	int stopx = image_x + fx / 2;
	int stopy = image_y + fy / 2;

	float isum = 0;
	int ix = 0, iy = 0;
	for (int x = sx; x <= stopx; x++)
	{
		for (int y = sy; y <= stopy; y++)
		{
			//do rgb
			if (rgb != -1)
			{
				if (rgb == RED)
				{
					float c = (float)getPixel(x, y, RED) / 255.0f;
					isum += (float)filter[ix][iy] * c;
				}
				else if (rgb == GREEN)
				{
					float c = (float)getPixel(x, y, GREEN) / 255.0f;
					isum += (float)filter[ix][iy] * c;
				}
				else 
				{
					float c = (float)getPixel(x, y, BLUE) / 255.0f;
					isum += (float)filter[ix][iy] * c;
				}
			}
			//do hsi
			else
			{
				HSI hsi_vector = getColor(x, y);
				if (hsi == HUE)
				{
					isum += (hsi_vector.h / 359.0f) * (float)filter[ix][iy];
				}
				else if (hsi == SATURATION)
				{
					isum += hsi_vector.s * (float)filter[ix][iy];
				}
				else
				{
					isum += hsi_vector.i * (float)filter[ix][iy];
				}
			}

			iy++;
		}
		iy = 0;
		ix++;
	}

	isum = isum / (float)fsum;
	return isum;
}

//returns result of filter, no bounds checking. Clamps values 0 to 1
float image::filter_abs_3x3(int filter[3][3], int image_x, int image_y, int fx, int fy, int rgb, int hsi)
{
	if (fx % 2 == 0 || fy % 2 == 0)
	{
		cout << "Must use odd filter size\n";
		exit(0);
	}

	int fsum = 0;
	for (int i = 0; i < fx; i++)
	{
		for (int j = 0; j < fy; j++)
		{
			fsum += abs(filter[i][j]);
		}
	}

	int sx = image_x - fx / 2;
	int sy = image_y - fy / 2;
	int stopx = image_x + fx / 2;
	int stopy = image_y + fy / 2;

	float isum = 0;
	int ix = 0, iy = 0;
	for (int x = sx; x <= stopx; x++)
	{
		for (int y = sy; y <= stopy; y++)
		{
			//do rgb
			if (rgb != -1)
			{
				if (rgb == RED)
				{
					float c = (float)getPixel(x, y, RED) / 255.0f;
					isum += (float)filter[ix][iy] * c;
				}
				else if (rgb == GREEN)
				{
					float c = (float)getPixel(x, y, GREEN) / 255.0f;
					isum += (float)filter[ix][iy] * c;
				}
				else
				{
					float c = (float)getPixel(x, y, BLUE) / 255.0f;
					isum += (float)filter[ix][iy] * c;
				}
			}
			//do hsi
			else
			{
				HSI hsi_vector = getColor(x, y);
				if (hsi == HUE)
				{
					isum += (hsi_vector.h / 359.0f) * (float)filter[ix][iy];
				}
				else if (hsi == SATURATION)
				{
					isum += hsi_vector.s * (float)filter[ix][iy];
				}
				else
				{
					isum += hsi_vector.i * (float)filter[ix][iy];
				}
			}

			iy++;
		}
		iy = 0;
		ix++;
	}

	isum = isum / (float)fsum;
	return isum;
}

//returns true if the input filter width/height can fit at the current row/col
bool image::filter_fits(int fx, int fy, int i, int j)
{
	if (fx % 2 == 0 || fy %2 == 0) 
	{
		cout << "Must use odd filter size\n";
		exit(0);
	}

	int fw = fx / 2; int fh = fy / 2;

	if (i - fw < 0 || i + fw >= Width()) { return false; }
	if (j - fh < 0 || j + fh >= Height()) { return false; }

	return true;
}



