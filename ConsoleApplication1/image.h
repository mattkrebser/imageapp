
#ifndef IMAGE_H
#define IMAGE_H
#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <vector>

using namespace std;

enum channel { RED, GREEN, BLUE, GREY = RED, GRAY = GREY };
enum hsiType { HUE, SATURATION, INTENSITY};

struct imageData
{
	vector<vector<int>>  redChannel, greenChannel, blueChannel;
	int width, height;
};

struct RGB 
{
	int r, g, b;
	RGB();
	RGB(int r_, int b_, int g_);
	static float distance(RGB color1, RGB color2);

	RGB& operator=(const RGB& color)
	{
		r = color.r;
		g = color.g;
		b = color.b;
		return *this;
	}
};
struct HSI
{
	float h, s, i;

	HSI();
	HSI(float r_, float b_, float g_);

	HSI& operator=(const HSI& color)
	{
		h = color.h;
		s = color.s;
		i = color.i;
		return *this;
	}

	static float deg2Rad(float deg)
	{
		return deg * 3.14159265359f / 180.0f;
	}
	static float rad2Deg(float rad)
	{
		return rad * 180.0f / 3.14159265359f;
	}
};

struct PPMObject
{
	std::string magicNum;
	int width, height, maxColVal;
	char * m_Ptr;
};

struct ROI
{
	int x, y;
	int l, h;
	ROI(int x_, int y_, int l_, int h_)
	{
		x = x_; y = y_; l = l_; h = h_;
	}
	bool PointInsideROI(int i, int j)
	{
		return (i >= x && i < x + l && j >= y && j < y + h);
	}
	static bool PointInsideAnyROI(int i, int j, vector<ROI> roi)
	{
		for (size_t n = 0; n < roi.size(); n++)
		{
			if (roi[n].PointInsideROI(i, j))
				return true;
		}
		return false;
	}
};

class image
{
private:
	imageData data;
	void eat_comment(istream &f);
	void readIn(istream &inputStream, PPMObject &other);
	void saveOut(ostream &outputStream, const PPMObject &other);

public:
	image();
	image(image &img);
	~image();

	bool isPPM;

	void deleteImage();
	void copyImage(image &img);
	void resize(int width, int height);
	void setPixel(const int i, const int j, const int value);
	void setPixel(const int i, const int j, const int rgb, const int value);
	void setColor(const int ix, const int iy, const HSI value);

	int getPixel(const int i, const int j);
	int getPixel(const int i, const int j, const int rgb);
	HSI getColor(const int i, const int j);
	int Width();
	int Height();

	bool save(char* file);
	bool save(const char* file);
	bool read(char* file);
	bool isInbounds(const int i, const int j);

	int Average(const int x, const int y, const int rgb, int width);
	void setPixels(const int x, const int y, const int rgb, const int width, const int height, const int value);

	RGB getColorVector(const int i, const int j);
	void setColorVector(const int i, const int j, RGB value);
	int getPixelAvg(const int ix, const int iy, const int x, const int y, int rgb);
	int getPixelSum(const int ix, const int iy, const int x, const int y, int rgb);

	bool ChannelIsEmpty(int rgb);

	vector<float> * getPMF(const int x, const int y, const int rowlen, const int collen, int rgb = 0, int hsi = -1);

	float filter_abs_5x5(int filter[5][5], int image_x ,int image_y, int fx, int fy, int rgb, int hsi);
	float filter_abs_3x3(int filter[3][3], int image_x, int image_y, int fx, int fy, int rgb, int hsi);
	bool filter_fits(int fx, int fy, int i, int j);
};

#endif

