#ifndef UTILITY_H
#define UTILITY_H

#include "image.h"
#include <sstream>

class utility
{
public:
	static void addGrey(image &src, image &tgt, int value);
	static void Threshold(image &src, image &tgt, int value);
	static void Scale(image &src, image &tgt, int factor);
	static void DirectoryScale(int factor, char * dirName, char * outputDirName);
	static void DirectoryThreshold(int value, char * dirName, char * outputDirName);
	static void ColorBinarize(image &src, image &tgt, RGB color, int distance);
	static void Smooth(image &src, image &tgt, int SmoothSize);
	static void SeperableSmooth(image &src, image &tgt, int SmoothSize);
	static void IncrementalSmooth(image &src, image &tgt, int SmoothSize);
	static void IncrementalSmooth(image &src, image &tgt, int SmoothSize, vector<ROI> &roi);
	static void SeperableSmooth(image &src, image &tgt, int SmoothSize, vector<ROI> &roi);
	static void Smooth(image &src, image &tgt, int SmoothSize, vector<ROI> &roi);
	static void ColorBinarize(image &src, image &tgt, RGB color, int distance, vector<ROI> &roi);
	static void Threshold(image &src, image &tgt, int value, vector<ROI> &roi);
	static void MakeHistograms(image &src, string imgName, int numberDivisions = 0, int rgb = 0, int hsi = -1);
	static void HistogramEqualization(image &src, image &tgt, int rgb = 0, int hsi = -1, int numDivisions = 0);
	static void CopyEmptyChannels(image &src, image &tgt);
	static void AddHSI(image &src, image &tgt, HSI value);
	static void Stretch(image &src, image &tgt);
	static void EdgeDetect(image &src, image &tgt, image &t, image &dir, vector<ROI> &roi, int threshold,
		int dir_threshold, int rgb = 0, int hsi = -1, bool _5x5 = true);
};

#endif

