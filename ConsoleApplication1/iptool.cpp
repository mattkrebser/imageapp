#include "stdafx.h"
#include "image.h"
#include "utility.h"
#include <iostream>
#include <string>
#include <opencv2\imgproc\imgproc.hpp>
#include "opencv2/highgui/highgui.hpp"


using namespace std;
using namespace cv;

static void DoROI(char** argv, int argc);
static void fillROIList(char ** argv, vector<ROI> &roi, int offset, int argc);
static int Contains(vector<string> &list, char * value);
static void Equalize(char ** argv, int argc, image &src, image &tgt);
static void Insert(string &name, char * insert);
static void DoEdgeDetect(char ** argv, int argc, image &src);
static int GetPosition(vector<string> list, char * value);
static void OpenCV(char ** argv, int argc);
static void CopyToImg(Mat &mat, image &img);
static void CopyFromImg(Mat &mat, image &img);

int main(int argc, char** argv)
{
	if (strcmp(argv[3], "DirectoryScale") == 0)
	{
		utility::DirectoryScale(atoi(argv[4]), argv[1], argv[2]);
		return 0;
	}

	if (strcmp(argv[3], "DirectoryThreshold") == 0)
	{
		utility::DirectoryThreshold(atoi(argv[4]), argv[1], argv[2]);
		return 0;
	}
	
	image src, tgt;
	src.read(argv[1]);
	tgt.isPPM = src.isPPM;

	if (strcmp(argv[3], "ROI") == 0) {

		DoROI(argv, argc);
		return 0;
	}
	else if (strcmp(argv[3], "Add") == 0) {

		utility::addGrey(src, tgt, atoi(argv[4]));
	}
	else if (strcmp(argv[3], "Binarize") == 0) {

		// do binarization
		utility::Threshold(src, tgt, atoi(argv[4]));
	}
	else if (strcmp(argv[3], "ColorBinarize") == 0) {

		RGB color(atoi(argv[4]), atoi(argv[5]), atoi(argv[6]));
		utility::ColorBinarize(src, tgt, color, atoi(argv[7]));
	}
	else if (strcmp(argv[3], "Scale") == 0) {

		// do scaling
		utility::Scale(src, tgt, atoi(argv[4]));
	}
	else if (strcmp(argv[3], "Smooth") == 0) {

		// do smoothscaling
		utility::Smooth(src, tgt, atoi(argv[4]));
	}
	else if (strcmp(argv[3], "SeperableSmooth") == 0) {

		// do smoothscaling
		utility::SeperableSmooth(src, tgt, atoi(argv[4]));
	}
	else if (strcmp(argv[3], "IncrementalSmooth") == 0) {

		// do smoothscaling
		utility::IncrementalSmooth(src, tgt, atoi(argv[4]));
	}
	else if (strcmp(argv[3], "Equalize") == 0) {

		Equalize(argv, argc, src, tgt);
	}
	else if (strcmp(argv[3], "AddHSI") == 0)
	{
		HSI value;
		float h = (float)atoi(argv[4]);
		float s = (float)atoi(argv[5]);
		float i = (float)atoi(argv[6]);

		s = s / 360.0f;
		i = i / 360.0f;

		value.h = h; value.s = s; value.i = i;

		utility::AddHSI(src, tgt, value);
	}
	else if (strcmp(argv[3], "Stretch") == 0)
	{
		string name(argv[1]);
		utility::MakeHistograms(src, name, 0, RED, -1);
		utility::Stretch(src, tgt);
		Insert(name, "-Stretched");
		utility::MakeHistograms(tgt, name, 0, RED, -1);
	}
	else if (strcmp(argv[2], "EdgeDetect") == 0)
	{
		DoEdgeDetect(argv, argc, src); return 0;
	}
	else if (strcmp(argv[3], "OCV") == 0)
	{
		OpenCV(argv, argc); return 0;
	}
	else 
	{
		cout << "Unrecognized command: " << argv[3] <<"\n";
	}

	tgt.save(argv[2]);
}

static void Equalize(char ** argv, int argc, image &src, image &tgt)
{
	vector<string> args;
	for (int i = 0; i < argc; i++)
	{
		string * val = new string(argv[i]);
		args.push_back(*val);
	}

	bool Modified = false;

	if (Contains(args, "-R") == 1)
	{
		bool localModified = false;
		Modified = true;
		//quarters equalization
		if (Contains(args, "-Q") == 1)
		{
			//histogram option on
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-RED"); localModified = true;
				utility::MakeHistograms(src, name, 1, RED, -1);
			}
			utility::HistogramEqualization(src, tgt, RED, -1, 1);
			//histogram option on
			if (Contains(args, "-HG") == 1 && localModified == true) {
				string name(argv[1]); Insert(name, "-RED-Equalized");
				utility::MakeHistograms(tgt, name, 1, RED, -1);
			}
		}
		//full image
		else 
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-RED"); localModified = true;
				utility::MakeHistograms(src, name, 0, RED, -1);
			}
			utility::HistogramEqualization(src, tgt, RED, -1, 0);
			if (Contains(args, "-HG") == 1 && localModified == true) {
				string name(argv[1]); Insert(name, "-RED-Equalized");
				utility::MakeHistograms(tgt, name, 0, RED, -1);
			}
		}
		cout << "Finished Red Channel Equalization...\n";
	}
	if (Contains(args, "-G") == 1)
	{
		bool localModified = false;
		Modified = true;
		if (Contains(args, "-Q") == 1)
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-GREEN"); localModified = true;
				utility::MakeHistograms(src, name, 1, GREEN, -1);
			}
			utility::HistogramEqualization(src, tgt, GREEN, -1, 1);
			if (Contains(args, "-HG") == 1 && localModified == true) {
				string name(argv[1]); Insert(name, "-GREEN-Equalized");
				utility::MakeHistograms(tgt, name, 1, GREEN, -1);
			}
		}
		else
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-GREEN"); localModified = true;
				utility::MakeHistograms(src, name, 0, GREEN, -1);
			}
			utility::HistogramEqualization(src, tgt, GREEN, -1, 0);
			if (Contains(args, "-HG") == 1 && localModified == true) {
				string name(argv[1]); Insert(name, "-GREEN-Equalized");
				utility::MakeHistograms(tgt, name, 0, GREEN, -1);
			}
		}
		cout << "Finished Green Channel Equalization...\n";
	}
	if (Contains(args, "-B") == 1)
	{
		bool localModified = false;
		Modified = true;
		if (Contains(args, "-Q") == 1)
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-BLUE"); localModified = true;
				utility::MakeHistograms(src, name, 1, BLUE, -1);
			}
			utility::HistogramEqualization(src, tgt, BLUE, -1, 1);
			if (Contains(args, "-HG") == 1 && localModified == true) {
				string name(argv[1]); Insert(name, "-BLUE-Equalized");
				utility::MakeHistograms(tgt, name, 1, BLUE, -1);
			}
		}
		else
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-BLUE"); localModified = true;
				utility::MakeHistograms(src, name, 0, BLUE, -1);
			}
			utility::HistogramEqualization(src, tgt, BLUE, -1, 0);
			if (Contains(args, "-HG") == 1 && localModified == true) {
				string name(argv[1]); Insert(name, "-BLUE-Equalized");
				utility::MakeHistograms(tgt, name, 0, BLUE, -1);
			}
		}
		cout << "Finished Blue Channel Equalization...\n";
	}
	if (Contains(args, "-H") == 1)
	{
		bool localModified = false;
		Modified = true;
		if (Contains(args, "-Q") == 1)
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-HUE"); 
				utility::MakeHistograms(src, name, 1, -1, HUE);
			}
			utility::HistogramEqualization(src, tgt, -1, HUE, 1);
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-HUE-Equalized");
				utility::MakeHistograms(tgt, name, 1, -1, HUE);
			}
		}
		else
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-HUE");
				utility::MakeHistograms(src, name, 0, -1, HUE);
			}
			utility::HistogramEqualization(src, tgt, -1, HUE, 0);
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-HUE-Equalized");
				utility::MakeHistograms(tgt, name, 0, -1, HUE);
			}
		}
		cout << "Finished Hue Equalization...\n";
	}
	if (Contains(args, "-S") == 1)
	{
		bool localModified = false;
		Modified = true;
		if (Contains(args, "-Q") == 1)
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-SAT");
				utility::MakeHistograms(src, name, 1, -1, SATURATION);
			}
			if (Contains(args, "-H"))
				utility::HistogramEqualization(tgt, tgt, -1, SATURATION, 1);
			else
				utility::HistogramEqualization(src, tgt, -1, SATURATION, 1);
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-SAT-Equalized");
				utility::MakeHistograms(tgt, name, 1, -1, SATURATION);
			}
		}
		else
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-SAT");
				utility::MakeHistograms(src, name, 0, -1, SATURATION);
			}
			if (Contains(args, "-H"))
				utility::HistogramEqualization(tgt, tgt, -1, SATURATION, 0);
			else
				utility::HistogramEqualization(src, tgt, -1, SATURATION, 0);
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-SAT-Equalized");
				utility::MakeHistograms(tgt, name, 0, -1, SATURATION);
			}
		}
		cout << "Finished Saturation Equalization...\n";
	}
	if (Contains(args, "-I") == 1)
	{
		bool localModified = false;
		Modified = true;
		if (Contains(args, "-Q") == 1)
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-INT");
				utility::MakeHistograms(src, name, 1, -1, INTENSITY);
			}
			if (Contains(args, "-H") || Contains(args, "-S"))
				utility::HistogramEqualization(tgt, tgt, -1, INTENSITY, 1);
			else
				utility::HistogramEqualization(src, tgt, -1, INTENSITY, 1);
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-INT-Equalized");
				utility::MakeHistograms(tgt, name, 1, -1, INTENSITY);
			}
		}
		else
		{
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-INT");
				utility::MakeHistograms(src, name, 0, -1, INTENSITY);
			}
			if (Contains(args, "-H") || Contains(args, "-S"))
				utility::HistogramEqualization(tgt, tgt, -1, INTENSITY, 0);
			else
				utility::HistogramEqualization(src, tgt, -1, INTENSITY, 0);
			if (Contains(args, "-HG") == 1) {
				string name(argv[1]); Insert(name, "-INT-Equalized");
				utility::MakeHistograms(tgt, name, 0, -1, INTENSITY);
			}
		}
		cout << "Finished Intensity Equalization...\n";
	}

	utility::CopyEmptyChannels(src, tgt);

	//histogram option on and the image was equalized
	if (Contains(args, "-HG") == 1 && Modified == false)
	{
		if (Contains(args, "-Q") == 1)
		{
			string name(argv[1]);
			utility::MakeHistograms(src, name, 1, RED, -1);
		}
		else
		{
			string name(argv[1]);
			utility::MakeHistograms(src, name, 0, RED, -1);
		}
	}
}

//appends something to the input string without removing the .ppm or .pgm
static void Insert(string &name, char * insert)
{
	size_t pos = name.find(".ppm");
	size_t pos1 = name.find(".pgm");

	if (name.find(".ppm") != std::string::npos)
	{
		name = name.substr(0, pos);
	}
	else
	{
		name = name.substr(0, pos1);
	}

	name.append(insert);

	if (name.find(".ppm") != std::string::npos) name.append(".ppm"); else name.append(".pgm");
}

//returns 1 if the input list contains the input value
static int Contains(vector<string> &list, char * value)
{
	for (size_t i = 0; i < list.size(); i++)
	{
		if (list[i].compare(value) == 0)
			return 1;
	}
	return 0;
}

//return position of input char * in the input list. Returns -1 if not found
static int GetPosition(vector<string> list, char * value)
{
	for (size_t i = 0; i < list.size(); i++)
	{
		if (list[i].compare(value) == 0)
			return (int)i;
	}
	return -1;
}

static void DoROI(char** argv, int argc)
{
	if (argc < 9)
	{
		cout << "Invalid input\n";
		return;
	}

	vector<ROI> roi;

	image src, tgt;
	src.read(argv[1]);
	tgt.isPPM = src.isPPM;

	if (strcmp(argv[4], "Binarize") == 0) {
		fillROIList(argv, roi, 6, argc);
		// do binarization
		utility::Threshold(src, tgt, atoi(argv[5]), roi);
	}
	else if (strcmp(argv[4], "ColorBinarize") == 0) {
		fillROIList(argv, roi, 9, argc);
		RGB color(atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
		utility::ColorBinarize(src, tgt, color, atoi(argv[8]), roi);
	}
	else if (strcmp(argv[4], "Smooth") == 0) {
		fillROIList(argv, roi, 6, argc);
		// do smoothscaling
		utility::Smooth(src, tgt, atoi(argv[5]), roi);
	}
	else if (strcmp(argv[4], "SeperableSmooth") == 0) {
		fillROIList(argv, roi, 6, argc);
		// do smoothscaling
		utility::SeperableSmooth(src, tgt, atoi(argv[5]), roi);
	}
	else if (strcmp(argv[4], "IncrementalSmooth") == 0) {
		fillROIList(argv, roi, 6, argc);
		// do smoothscaling
		utility::IncrementalSmooth(src, tgt, atoi(argv[5]), roi);
	}
	else
	{
		cout << "Unrecognized command!\n";
	}

	tgt.save(argv[2]);
}

static void fillROIList(char ** argv, vector<ROI> &roi, int offset, int argc)
{
	int numRegions = atoi(argv[offset]);
	if (numRegions * 4 + offset >= argc)
	{
		cout << "Invalid input, too few arguemnts.\n";
		exit(1);
	}
	for (int i = 0; i < numRegions * 4; i+=4)
	{
		ROI r1(atoi(argv[offset + 1 + i]), atoi(argv[offset + 2 + i]), atoi(argv[offset + 3 + i]), atoi(argv[offset + 4 + i]));
		roi.push_back(r1);
	}
}

static void DoEdgeDetect(char ** argv, int argc, image &src)
{
	vector<string> args;
	for (int i = 0; i < argc; i++)
	{
		string * val = new string(argv[i]);
		args.push_back(*val);
	}

	//determne if 3x3 or 5x5
	bool _3x3 = false;
	if (Contains(args, "-3x3"))
		_3x3 = true;

	//build roi list
	vector<ROI> roi;
	if (Contains(args, "-ROI"))
	{
		int pos = GetPosition(args, "-ROI");
		if (pos >= 0)
		{
			pos++;
			for (int i = pos; i < argc; pos += 4)
			{
				if (pos + 3 >= argc)
					break;
				ROI r_o_i(atoi(argv[pos]), atoi(argv[pos + 1]), atoi(argv[pos + 2]), atoi(argv[pos + 3]));
				roi.push_back(r_o_i);
			}
		}
	}

	//get thresholds
	int t = atoi(argv[3]);
	int td = atoi(argv[4]);

	//do edgedetection
	if (Contains(args, "-R") || Contains(args, "-G") || Contains(args, "-B"))
	{
		image g_r, t_r, d_r;
		g_r.isPPM = src.isPPM; t_r.isPPM = src.isPPM; d_r.isPPM = src.isPPM;
		image g_g, t_g, d_g;
		g_g.isPPM = src.isPPM; t_g.isPPM = src.isPPM; d_g.isPPM = src.isPPM;
		image g_b, t_b, d_b;
		g_b.isPPM = src.isPPM; t_b.isPPM = src.isPPM; d_b.isPPM = src.isPPM;

		if (Contains(args, "-R"))
		{
			cout << "Starting Red Channel Edge Detect...";
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_r"); Insert(f2, "_gradient_thresholded_r"); Insert(f3, "_gradient_thresholded_direction_r");

			utility::EdgeDetect(src, g_r, t_r, d_r, roi, t, td, RED, -1, !_3x3);
			g_r.save(f1.c_str());
			t_r.save(f2.c_str());
			d_r.save(f3.c_str());
			cout << "Done\n";
		}
		if (Contains(args, "-G"))
		{
			cout << "Starting Green Channel Edge Detect...";
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_g"); Insert(f2, "_gradient_thresholded_g"); Insert(f3, "_gradient_thresholded_direction_g");

			utility::EdgeDetect(src, g_g, t_g, d_g, roi, t, td, GREEN, -1, !_3x3);
			g_g.save(f1.c_str());
			t_g.save(f2.c_str());
			d_g.save(f3.c_str());
			cout << "Done\n";
		}
		if (Contains(args, "-B"))
		{
			cout << "Starting Blue Channel Edge Detect...";
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_b"); Insert(f2, "_gradient_thresholded_b"); Insert(f3, "_gradient_thresholded_direction_b");

			utility::EdgeDetect(src, g_b, t_b, d_b, roi, t, td, BLUE, -1, !_3x3);
			g_b.save(f1.c_str());
			t_b.save(f2.c_str());
			d_b.save(f3.c_str());
			cout << "Done\n";
		}
		if (Contains(args, "-C"))
		{
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_combined_rgb"); Insert(f2, "_gradient_thresholded_combined_rgb"); Insert(f3, "_gradient_thresholded_direction_combined_rgb");

			cout << "Starting Combine...";
			image g_c, t_c, d_c;
			g_c.isPPM = src.isPPM; t_c.isPPM = src.isPPM; d_c.isPPM = src.isPPM;

			g_c.resize(src.Width(), src.Height()); t_c.resize(src.Width(), src.Height()); d_c.resize(src.Width(), src.Height());
			int r = Contains(args, "-R"), b = Contains(args, "-B"), g = Contains(args, "-G");

			for (int i = 0; i < src.Width(); i++)
			{
				for (int j = 0; j < src.Height(); j++)
				{
					if (r)
					{
						g_c.setPixel(i, j, RED, g_r.getPixel(i, j, RED));
						t_c.setPixel(i, j, RED, t_r.getPixel(i, j, RED));
						d_c.setPixel(i, j, RED, d_r.getPixel(i, j, RED));
					}
					if (g)
					{
						g_c.setPixel(i, j, GREEN, g_g.getPixel(i, j, GREEN));
						t_c.setPixel(i, j, GREEN, t_g.getPixel(i, j, GREEN));
						d_c.setPixel(i, j, GREEN, d_g.getPixel(i, j, GREEN));
					}
					if (b)
					{
						g_c.setPixel(i, j, BLUE, g_b.getPixel(i, j, BLUE));
						t_c.setPixel(i, j, BLUE, t_b.getPixel(i, j, BLUE));
						d_c.setPixel(i, j, BLUE, d_b.getPixel(i, j, BLUE));
					}
				}
			}
			g_c.save(f1.c_str());
			t_c.save(f2.c_str());
			d_c.save(f3.c_str());
			cout << "Done\n";
		}
	}
	else if (Contains(args, "-H") || Contains(args, "-S") || Contains(args, "-I"))
	{
		image g_h, t_h, d_h;
		g_h.isPPM = src.isPPM; t_h.isPPM = src.isPPM; d_h.isPPM = src.isPPM;
		image g_s, t_s, d_s;
		g_s.isPPM = src.isPPM; t_s.isPPM = src.isPPM; d_s.isPPM = src.isPPM;
		image g_i, t_i, d_i;
		g_i.isPPM = src.isPPM; t_i.isPPM = src.isPPM; d_i.isPPM = src.isPPM;

		if (Contains(args, "-H"))
		{
			cout << "Starting Hue Channel Edge Detect...";
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_h"); Insert(f2, "_gradient_thresholded_h"); Insert(f3, "_gradient_thresholded_direction_h");

			utility::EdgeDetect(src, g_h, t_h, d_h, roi, t, td, -1, HUE, !_3x3);
			g_h.save(f1.c_str());
			t_h.save(f2.c_str());
			d_h.save(f3.c_str());
			cout << "Done\n";
		}
		if (Contains(args, "-S"))
		{
			cout << "Starting Saturation Channel Edge Detect...";
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_s"); Insert(f2, "_gradient_thresholded_s"); Insert(f3, "_gradient_thresholded_direction_s");

			utility::EdgeDetect(src, g_s, t_s, d_s, roi, t, td, -1, SATURATION, !_3x3);
			g_s.save(f1.c_str());
			t_s.save(f2.c_str());
			d_s.save(f3.c_str());
			cout << "Done\n";
		}
		if (Contains(args, "-I"))
		{
			cout << "Starting Intensity Channel Edge Detect...";
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_i"); Insert(f2, "_gradient_thresholded_i"); Insert(f3, "_gradient_thresholded_direction_i");

			utility::EdgeDetect(src, g_i, t_i, d_i, roi, t, td, -1, INTENSITY, !_3x3);
			g_i.save(f1.c_str());
			t_i.save(f2.c_str());
			d_i.save(f3.c_str());
			cout << "Done\n";
		}
		/*if (Contains(args, "-C"))
		{
			string f1(argv[1]), f2(argv[1]), f3(argv[1]);
			Insert(f1, "_gradient_combined_hsi"); Insert(f2, "_gradient_thresholded_combined_hsi"); Insert(f3, "_gradient_thresholded_direction_combined_hsi");

			cout << "Starting Combine...";
			image g_c, t_c, d_c;
			g_c.isPPM = src.isPPM; t_c.isPPM = src.isPPM; d_c.isPPM = src.isPPM;

			g_c.resize(src.Width(), src.Height()); t_c.resize(src.Width(), src.Height()); d_c.resize(src.Width(), src.Height());
			int h = Contains(args, "-H"), s = Contains(args, "-S"), in = Contains(args, "-I");
			for (int i = 0; i < src.Width(); i++)
			{
				for (int j = 0; j < src.Height(); j++)
				{
					if (in)
					{
						float ivalg = g_i.getColor(i, j).i; float ivalt = t_i.getColor(i, j).i; float ivald = d_i.getColor(i, j).i;
						g_c.setColor(i, j, HSI(0, 0, ivalg));
						t_c.setColor(i, j, HSI(0, 0, ivalt));
						d_c.setColor(i, j, HSI(0, 0, ivald));
					}
					else
					{
						g_c.setColor(i, j, HSI(0, 0, src.getColor(i, j).i));
						float t_ih = t_h.getColor(i, j).i; float t_is = t_s.getColor(i, j).i;
						t_c.setColor(i, j, HSI(0, 0, t_ih > t_is ? t_ih : t_is));
						float d_ih = t_h.getColor(i, j).i; float d_is = d_s.getColor(i, j).i;
						d_c.setColor(i, j, HSI(0, 0, d_ih > d_is ? d_ih : d_is));
					}
					if (h)
					{
						float hvalg = g_h.getColor(i, j).h; float hvalt = t_h.getColor(i, j).h; float hvald = d_h.getColor(i, j).h;
						HSI c_g = g_c.getColor(i, j); HSI c_t = t_c.getColor(i, j); HSI c_d = d_c.getColor(i, j);
						g_c.setColor(i, j, HSI(hvalg, 0, c_g.i));
						t_c.setColor(i, j, HSI(hvalt, 0, c_t.i));
						d_c.setColor(i, j, HSI(hvald, 0, c_d.i));
					}
					if (s)
					{
						float svalg = g_h.getColor(i, j).s; float svalt = t_s.getColor(i, j).s; float svald = d_s.getColor(i, j).s;
						HSI c_g = g_c.getColor(i, j); HSI c_t = t_c.getColor(i, j); HSI c_d = d_c.getColor(i, j);
						g_c.setColor(i, j, HSI(c_g.h, svalg, c_g.i));
						g_c.setColor(i, j, HSI(c_g.h, svalg, c_t.i));
						g_c.setColor(i, j, HSI(c_g.h, svalg, c_d.i));
					}
				}
			}
			g_c.save(f1.c_str());
			t_c.save(f2.c_str());
			d_c.save(f3.c_str());
			cout << "Done\n";
		}*/
	}
	else
	{
		cout << "Please include atleast one of '-H,-S,-I,-R,-G,-B' to do edge detection on.\n";
		exit(0);
	}
}

static void OpenCV(char ** argv, int argc)
{
	vector<string> args;
	for (int i = 0; i < argc; i++)
	{
		string * val = new string(argv[i]);
		args.push_back(*val);
	}

	//build roi list
	vector<ROI> roi;
	if (Contains(args, "-ROI"))
	{
		int pos = GetPosition(args, "-ROI");
		if (pos >= 0)
		{
			pos++;
			for (int i = pos; i < argc; pos += 4)
			{
				if (pos + 3 >= argc)
					break;
				ROI r_o_i(atoi(argv[pos]), atoi(argv[pos + 1]), atoi(argv[pos + 2]), atoi(argv[pos + 3]));
				roi.push_back(r_o_i);
			}
		}
	}

	//more input validation
	if (Contains(args, "-Eq") == 1)
	{
		//if splitting into quarters
		if (Contains(args, "-Q"))
		{
			//initalize
			image src; src.read(argv[1]);
			image ul, ur, ll, lr;
			ul.isPPM = ur.isPPM = ll.isPPM = lr.isPPM = false;
			int w = src.Width() / 2, h = src.Height() / 2;
			ul.resize(w, h); ur.resize(w, h); lr.resize(w, h); ll.resize(w, h);

			//copy into 4 images
			for (int i = 0; i < src.Width(); i++)
				for (int j = 0; j < src.Height(); j++)
				{
					if (i < w && j < h) ll.setPixel(i, j, src.getPixel(i, j));
					else if (i < w && j >= h) ul.setPixel(i, j - h, src.getPixel(i, j));
					else if (i >= w && j < h) lr.setPixel(i - w, j, src.getPixel(i, j));
					else ur.setPixel(i - w, j - h, src.getPixel(i, j));
				}

			Mat m_ur(h, w, CV_8U);Mat m_ul(h, w, CV_8U);Mat m_lr(h, w, CV_8U);Mat m_ll(h, w, CV_8U);
			//copy to mat
			CopyFromImg(m_ur, ur);CopyFromImg(m_ul, ul); CopyFromImg(m_lr, lr); CopyFromImg(m_ll, ll);
			Mat d_ur, d_ul, d_lr, d_ll;

			//equalize
			equalizeHist(m_ur, d_ur); equalizeHist(m_ul, d_ul); equalizeHist(m_lr, d_lr); equalizeHist(m_ll, d_ll);

			//recombine and save equalized img
			image img_eq; img_eq.isPPM = false; img_eq.resize(src.Width(), src.Height());
			for (int i = 0; i < img_eq.Width(); i++)
				for (int j = 0; j < img_eq.Height(); j++)
				{
					if (i < w && j < h) img_eq.setPixel(i, j, d_ll.at<uchar>(j, i));
					else if (i < w && j >= h) img_eq.setPixel(i, j, d_ul.at<uchar>(j - h, i));
					else if (i >= w && j < h) img_eq.setPixel(i, j, d_lr.at<uchar>(j, i - w));
					else img_eq.setPixel(i, j, d_ur.at<uchar>(j - h, i - w));
				}
			img_eq.save(argv[2]);
			
			//save histograms
			string name(argv[1]); string name2(argv[2]); Insert(name2, "_equalized");
			utility::MakeHistograms(src, name, 1, RED, -1);
			utility::MakeHistograms(img_eq, name2, 1, RED, -1);
		}
		else
		{
			Mat src, dst;
			//read image
			string foutname(argv[2]);
			string finname(argv[1]);

			src = imread(finname, 0);

			image srcimg, dstimg;

			if (!src.data)
			{
				cout << "Error loading image!\n" << endl; exit(0);
			}

			//equalize and save
			equalizeHist(src, dst);

			//write img
			try {
				srcimg.isPPM = false;
				dstimg.isPPM = false;
				CopyToImg(src, srcimg);
				CopyToImg(dst, dstimg);
				dstimg.save(argv[2]);
			}
			catch (runtime_error& ex)
			{
				cout << "Error Saving image.\n";
				cout << ex.what() << "\n";
				exit(0);
			}

			Insert(finname, "_Histogram");
			Insert(foutname, "_Histogram");

			//do histo equalize
			utility::MakeHistograms(srcimg, finname, 0, RED, -1);
			utility::MakeHistograms(dstimg, foutname, 0, RED, -1);

		}
	}
	else if (Contains(args, "-Sobel"))
	{
		int t = 0, td = 0;
		t = atoi(argv[4]);
		td = atoi(argv[5]);

		Mat msrc;
		int ddepth = CV_32F;
		Point anchor = Point(3, 3);

		msrc = imread(argv[1], 0);
		Mat g_y, g_x;

		Mat Y = (Mat_<float>(5, 5) <<
			-5, -8, -10, -8, -5,
			-4, -10, -20, -10, -4,
			0, 0, 0, 0, 0,
			4, 10, 20, 10, 4,
			5, 8, 10, 8, 5) / 168.0f;
		Mat X = (Mat_<float>(5, 5) <<
			-5, -4, 0, 4, 5,
			-8, -10, 0, 10, 8,
			-10, -20, 0, 20, 10,
			-8, -10, 0, 10, 8,
			-5, -4, 0, 4, 5) / 168.0f;

		//opencv filter2D function, computes dx and dy
		filter2D(msrc, g_y, ddepth, Y, anchor, 0, BORDER_DEFAULT);
		filter2D(msrc, g_x, ddepth, X, anchor, 0, BORDER_DEFAULT);

		//our images
		image dst; dst.isPPM = false; dst.resize(msrc.cols, msrc.rows);
		image dst_t; dst_t.isPPM = false; dst_t.resize(msrc.cols, msrc.rows);
		image dst_td; dst_td.isPPM = false; dst_td.resize(msrc.cols, msrc.rows);

		//compute gradient and directions
		for (int i = 0; i < msrc.rows; i++)
			for (int j = 0; j < msrc.cols; j++)
			{
				float dy = g_y.at<float>(i, j); float dx = g_x.at<float>(i, j);
				//do gradient
				float g = sqrtf(dy * dy + dx * dx); dst.setPixel(j, i, (int)g);
				//do theta
				float theta = atan2f(dy, dx);theta = HSI::rad2Deg(theta);if (theta < 0) theta += 360.0f; dst_td.setPixel(j, i, (int)theta);
			}

		//do thresholdings
		for (int i = 0; i < dst.Width(); i++)
			for (int j = 0; j < dst.Height(); j++)
			{
				//gradient amplitude
				int v = dst.getPixel(i, j); dst.setPixel(i, j, v);
				//threshold
				dst_t.setPixel(i, j, v >= t ? 255 : 0);
				//direction threshold
				int d1 = td; int d2 = 180 + td; if (d2 >= 360) d2 -= 360; int thetai = dst_td.getPixel(i, j);
				if (abs(thetai - d1) <= 10 || abs(thetai - d1) >= 350) dst_td.setPixel(i, j, v >= t ? 255 : 0);
				else if (abs(thetai - d2) <= 10 || abs(thetai - d2) >= 350) dst_td.setPixel(i, j, v >= t ? 255 : 0);
				else dst_td.setPixel(i, j, 0);
			}

		//do ROI
		if (roi.size() > 0)
			for (int i = 0; i < dst.Width(); i++)
				for (int j = 0; j < dst.Height(); j++)
				{
					if (!ROI::PointInsideAnyROI(i, j, roi))
					{
						dst.setPixel(i, j, msrc.at<uchar>(j, i));
						dst_t.setPixel(i, j, msrc.at<uchar>(j, i));
						dst_td.setPixel(i, j, msrc.at<uchar>(j, i));
					}
				}

		//save
		string name2(argv[2]); string name3(argv[2]); Insert(name2, "Thresholded"); Insert(name3, "DirectionalThresholded");
		dst.save(argv[2]);
		dst_t.save(name2.c_str());
		dst_td.save(name3.c_str());
	}
	else if (Contains(args, "-Canny"))
	{
		Mat msrc = cv::imread(argv[1], 0);
		Mat canny;

		int t = atoi(argv[4]);
		int td = atoi(argv[5]);

		Canny(msrc, canny, 40, 160);

		image dst; dst.isPPM = false; CopyToImg(canny, dst);

		//do ROI
		if (roi.size() > 0)
			for (int i = 0; i < dst.Width(); i++)
				for (int j = 0; j < dst.Height(); j++)
				{
					if (!ROI::PointInsideAnyROI(i, j, roi))
					{
						dst.setPixel(i, j, msrc.at<uchar>(j, i));
					}
				}

		dst.save(argv[2]);
	}
	else
		cout << "Edge Detection must have an '-Sobel' or '-Canny' parameter for OpenCV mode.\n";
}

//covert grayscale opencv format to my format
static void CopyToImg(Mat &mat, image &img)
{
	img.resize(mat.cols, mat.rows);
	for (int i = 0; i < mat.cols; i++)
		for (int j = 0; j < mat.rows; j++)
			img.setPixel(i, j, mat.at<uchar>(j, i));
}

//copys from img to mat, assumes mat to be proper size
//only grayscale copy
static void CopyFromImg(Mat &mat, image &img)
{
	for (int i = 0; i < img.Width(); i++)
		for (int j = 0; j < img.Height(); j++)
			mat.at<uchar>(j, i) = img.getPixel(i, j);
}


